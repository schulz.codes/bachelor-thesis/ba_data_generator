package com.dschulz.datagenerator.Math;

import com.graphhopper.util.shapes.GHPoint;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by daniel on 08.08.16.
 */
public class FunctionsTest {

    GHPoint posA;
    GHPoint posB;
    GHPoint posC;

    double bearingAB=2.0926996315497313;
    double bearingAC=-1.8188350403290885;
    double bearingBC=-1.4945844190988087;

    double distanceAB=0.5032147821894206;
    double distanceAC=0.6808725838439311;
    double distanceBC=1.0994720553819022;

    @Before
    public void setUp(){
        posA=new GHPoint(50.6835136,10.9348888);
        posB=new GHPoint(50.68126,10.9410734);
        posC= new GHPoint(50.682012,10.925531);


    }

    @Test
    public void degreeBearing() throws Exception {
        double brnAB=Functions.DegreeBearing(posA,posB);
        assertTrue(brnAB>-Math.PI &&brnAB<Math.PI);
        double brnAC=Functions.DegreeBearing(posA,posC);
        assertTrue(brnAC>-Math.PI &&brnAC<Math.PI);
        double brnBC=Functions.DegreeBearing(posB,posC);
        assertTrue(brnBC>-Math.PI &&brnBC<Math.PI);
        assertEquals(brnAB,bearingAB,0.00000001);
        assertEquals(brnAC,bearingAC,0.00000001);
        assertEquals(brnBC,bearingBC,0.00000001);
    }

    @Test
    public void distance() throws Exception {
        double distAB=Functions.Distance(posA,posB);
        double distAC=Functions.Distance(posA,posC);
        double distBC=Functions.Distance(posB,posC);
        assertEquals(distAB,distanceAB,0.00000001);
        assertEquals(distAC,distanceAC,0.00000001);
        assertEquals(distBC,distanceBC,0.00000001);
    }

    @Test
    public void moveDistanceInDirection() throws Exception {
        double distanceLastRound=Functions.Distance(posA,posB);
        double movePerRound=0.0005;
        GHPoint curPos=posA;
        double brng=Functions.DegreeBearing(posA,posB);
        while(distanceLastRound>0.0005){
            curPos=Functions.MoveDistanceInDirection(curPos,brng,movePerRound);
            double distance=Functions.Distance(curPos,posB);
           assertTrue(distanceLastRound>distance);
            distanceLastRound=distance;
        }
    }

}