package com.dschulz.datagenerator.Visitor.Provider;

/**
 * Created by daniel on 11.05.16.
 */

import com.dschulz.datagenerator.Cluster.Cluster;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStation;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.graphhopper.util.shapes.GHPoint;
import com.dschulz.datagenerator.Const;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel on 07.05.16.
 */
public class TUHttpDataProvider implements DataProvider {
    static final Logger logger = Logger.getLogger("DataProvider");
    @Override
    public void createRecord(Visitor visitor) {
        if (!Const.BATCH_PROCESSING) {
            String log=visitor.getId()+":{lat"+visitor.getPosition().getLat()+";lon"+visitor.getPosition().getLon()+"}@"+visitor.getTimingId()+" Provider::Create";
            logger.info(log);
            ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
            if(visitor.getId()<Const.VISITOR_COUNT_UNDER_100){
                postParameters.add(new BasicNameValuePair("uid", "" + visitor.getId()));
            }else{
                postParameters.add(new BasicNameValuePair("uid", "" + (visitor.getId()+99)));
            }
            postParameters.add(new BasicNameValuePair("lat", "" + visitor.getPosition().getLat()));
            postParameters.add(new BasicNameValuePair("lon", "" + visitor.getPosition().getLon()));
            send(postParameters, Const.HTTP_PROVIDER_CREATE_URL);
        }
    }

    @Override
    public void updateRecord(Visitor visitor) {
        if (!Const.BATCH_PROCESSING) {

            String log=visitor.getId()+":{lat"+visitor.getPosition().getLat()+";lon"+visitor.getPosition().getLon()+"}@"+visitor.getTimingId()+" Provider::Update";
            logger.info(log);
            ArrayList<NameValuePair> postParameters = new ArrayList<NameValuePair>();
            if(visitor.getId()<Const.VISITOR_COUNT_UNDER_100){
                postParameters.add(new BasicNameValuePair("uid", "" + visitor.getId()));
            }else{
                postParameters.add(new BasicNameValuePair("uid", "" + (visitor.getId()+99)));
            }
            postParameters.add(new BasicNameValuePair("lat", "" + visitor.getPosition().getLat()));
            postParameters.add(new BasicNameValuePair("lon", "" + visitor.getPosition().getLon()));
            send(postParameters, Const.HTTP_PROVIDER_UPDATE_URL);
        }
    }

    @Override
    public void updateRecord(Visitor visitor, GHPoint correctedPosition) throws NotImplementedException{
        throw new NotImplementedException();
    }

    @Override
    public void finalizeRecord(Visitor visitor) {
        /**
         * Not Supported by TU-Server
         */
        String log=visitor.getId()+":{lat"+visitor.getPosition().getLat()+";lon"+visitor.getPosition().getLon()+"}@"+visitor.getTimingId()+" Provider::Finalize";
        logger.info(log);
    }

    @Override
    public void createTransmitterStations(List<TransmitterStation> stationList) throws NotImplementedException{
        throw new NotImplementedException();
    }

    @Override
    public void createCluster(Cluster rootCluster) throws NotImplementedException {
        throw new NotImplementedException();
    }

    private void send(ArrayList<NameValuePair> parameter, String url) {
        HttpClient httpClient = HttpClientPool.getClient();
        try {
            HttpPost request = new HttpPost(url);
            request.setEntity(new UrlEncodedFormEntity(parameter));
            CloseableHttpResponse response = (CloseableHttpResponse) httpClient.execute(request);
            response.close();
        } catch (Exception ex) {
            logger.error("http error", ex);
        } finally {
        }
    }

}
