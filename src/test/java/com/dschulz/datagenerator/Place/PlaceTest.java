package com.dschulz.datagenerator.Place;

import com.dschulz.datagenerator.App;
import com.dschulz.datagenerator.Cluster.Cluster;
import com.dschulz.datagenerator.Cluster.DirectedEdge;
import com.dschulz.datagenerator.SampleData;
import com.dschulz.datagenerator.Routing.Hopper;
import com.dschulz.datagenerator.TestClass;
import com.dschulz.datagenerator.Visitor.VisitorFactory;
import com.graphhopper.util.shapes.GHPoint;
import org.apache.log4j.BasicConfigurator;
import org.jgrapht.WeightedGraph;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Created by daniel on 08.05.16.
 */

public class PlaceTest extends TestClass {


    @Before
    public void setup() throws Exception {
        super.setUp();
    }



    @Test
    public void testEquals() {
        for (Map.Entry<GHPoint, Place> tPlaceEntry : SampleData.PLACES().entrySet()) {
            assertTrue(tPlaceEntry.getValue().equals(tPlaceEntry.getValue()));
        }
    }

    @Test
    public void testGetPosition() {
        for (Map.Entry<GHPoint, Place> tPlaceEntry : SampleData.PLACES().entrySet()) {
            assertEquals(tPlaceEntry.getKey(), tPlaceEntry.getValue().getPosition());
        }
    }


}
