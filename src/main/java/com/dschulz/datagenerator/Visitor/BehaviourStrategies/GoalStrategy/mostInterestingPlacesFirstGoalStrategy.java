package com.dschulz.datagenerator.Visitor.BehaviourStrategies.GoalStrategy;

import com.dschulz.datagenerator.Place.Place;
import com.dschulz.datagenerator.Place.PlaceFactory;
import com.dschulz.datagenerator.Statistics.Enum.GoalStrategy;
import com.dschulz.datagenerator.Statistics.Statistics;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.graphhopper.util.shapes.GHPoint;

import java.util.PriorityQueue;

/**
 * Created by daniel on 22.07.16.
 */
public class mostInterestingPlacesFirstGoalStrategy implements VisitorGoalStrategy{

    private PriorityQueue<Place> placePriorityQueue = new PriorityQueue<>();
    private boolean initialized=false;
    @Override
    public GHPoint findGoal(Visitor visitor) {
        long startTime=System.nanoTime();
        if(!initialized){
            for(Place place:PlaceFactory.getInstance().getPlaces().values()){
                placePriorityQueue.add(place);
            }
            initialized=true;
        }
        if (visitor.getInterestNeeded() <= visitor.getInterestReducePerTime()) {
            long endTime=System.nanoTime()-startTime;
            Statistics.getInstance().addGoalStrategyTime(this.getClass().getSimpleName(), GoalStrategy.NO_INTEREST_TIME,endTime);
            return visitor.getHome();
        }

        Place polledPlace=placePriorityQueue.poll();
        if(polledPlace!=null){
            long endTime=System.nanoTime()-startTime;
            Statistics.getInstance().addGoalStrategyTime(this.getClass().getSimpleName(),GoalStrategy.FOUND_GOAL_TIME,endTime);
            return polledPlace.getPosition();
        }else{
            long endTime=System.nanoTime()-startTime;
            Statistics.getInstance().addGoalStrategyTime(this.getClass().getSimpleName(),GoalStrategy.ALL_SEEN_TIME,endTime);
            return visitor.getHome();// hat alles gesehen
        }

    }

    @Override
    public String getName() {

        return "MIPF";
    }
}
