package com.dschulz.datagenerator.Visitor.BehaviourStrategies.GoalStrategy;

import com.dschulz.datagenerator.Visitor.Visitor;
import com.graphhopper.util.shapes.GHPoint;

/**
 * Created by daniel on 16.04.16.
 */
public interface VisitorGoalStrategy {
    GHPoint findGoal(Visitor visitor);

    String getName();
}
