package com.dschulz.datagenerator.Place;

import com.dschulz.datagenerator.Cluster.Algorithm.ClusteringAlgorithm;
import com.dschulz.datagenerator.Cluster.Algorithm.GraphClusteringAlgorithm;
import com.dschulz.datagenerator.Cluster.Cluster;
import com.dschulz.datagenerator.Cluster.DirectedEdge;
import com.dschulz.datagenerator.Cluster.Stategies.CalculatedLinkageStrategy;
import com.dschulz.datagenerator.Const;
import com.dschulz.datagenerator.Statistics.Statistics;
import com.dschulz.datagenerator.Visitor.Provider.DataProvider;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.graphhopper.util.shapes.GHPoint;
import org.apache.log4j.Logger;
import org.jgrapht.WeightedGraph;
import org.jgrapht.graph.SimpleWeightedGraph;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by daniel on 14.07.16.
 */
public class PlaceFactory {
    private AtomicLong idCounter = new AtomicLong(0);
    protected WeightedGraph<Place, DirectedEdge> G = new SimpleWeightedGraph<Place, DirectedEdge>(DirectedEdge.class);
    protected Cluster Clustered=null;
    protected  HashMap<GHPoint,Place> Places = new HashMap<GHPoint, Place>();
    static final Logger logger = Logger.getLogger(Place.class);
    private static PlaceFactory instance=null;




    public ScheduledExecutorService placeExecutor =null;

    // Verhindere die Erzeugung des Objektes über andere Methoden
    private PlaceFactory () {
    }
    public static synchronized PlaceFactory getInstance () {
        if(instance==null){
            instance=new PlaceFactory();
        }
        return instance;
    }

    public WeightedGraph<Place, DirectedEdge> getGraph(){
        return G;
    }
    public Cluster getClustered(){
        if(Clustered==null){
            logger.error("Cluster not Initialized");
            return null;
        }
        return Clustered;
    }
    public HashMap<GHPoint, Place> getPlaces() {
        return Places;
    }
    public AtomicLong getIdCounter(){return idCounter;}


    public Place getPlaceToPosition(GHPoint Position){
        if(getPlaces().containsKey(Position)){
            return getPlaces().get(Position);
        }else{
            return null;
        }
    }

    public HashMap<GHPoint, Place> InitPlaces(List<PlaceJson> placeObjects){
        Places = getPlacesMapFromPlaceJson(placeObjects);
        logger.info("Init Places");
        long start= System.currentTimeMillis();
        logger.info("Init PlaceGraph");
        G= makeGraph(new ArrayList<>(Places.values()));
        logger.info("Init PlaceGraph took "+(System.currentTimeMillis()-start)+" ms");
        ClusteringAlgorithm tCluster=new GraphClusteringAlgorithm();
        Clustered=tCluster.performWeightedClustering(G, new CalculatedLinkageStrategy());
        long end = System.currentTimeMillis()-start;
        logger.info("Init Places took "+end+" ms");
        try {
            DataProvider prov = Const.VISITOR_DATA_PROVIDER.newInstance();
            prov.createCluster(Clustered);
        } catch (NotImplementedException e){
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return Places;
    }


    public List<PlaceJson> parsePlaceJSON(String jsonLocation){
        String jsonData = "";
        BufferedReader br = null;
        try {
            String line;
            br = new BufferedReader(new FileReader(jsonLocation));
            while ((line = br.readLine()) != null) {
                jsonData += line + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null)
                    br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
        // System.out.println("File Content: \n" + jsonData);
        Type collectionType = new TypeToken<List<PlaceJson>>() {
        }.getType();
        Gson gson = new Gson();

        List<PlaceJson> placeJsonList = gson.fromJson(jsonData, collectionType);

        return placeJsonList;
    }

    public WeightedGraph<Place,DirectedEdge> makeGraph(List<Place> places){
        SimpleWeightedGraph<Place, DirectedEdge> graph =new SimpleWeightedGraph<Place, DirectedEdge>(DirectedEdge.class);
        for (int col = 0; col < places.size(); col++) {
            for (int row = col + 1; row < places.size(); row++) {
                Place placeL=places.get(row);
                Place placeR= places.get(col);
                DirectedEdge tEdge = new DirectedEdge(placeL,placeR);
                try {
                    graph.addVertex(placeL);
                    graph.addVertex(placeR);
                    graph.addEdge(placeL, placeR, tEdge);
                    graph.setEdgeWeight(tEdge, tEdge.getWeight());
                }catch(Exception e){
                    logger.error("No Vertex",e);
                }
            }
        }
        return graph;
    }

    private HashMap<GHPoint, Place> getPlacesMapFromPlaceJson(List<PlaceJson> placeObjects){
        HashMap<GHPoint, Place> places =new HashMap<>();
        for (PlaceJson tPlace: placeObjects){
            try{
                if(tPlace.getInterest()==null){
                    places.put(tPlace.getPosition(),new Place(idCounter.getAndIncrement(),tPlace.getPosition(),tPlace.getName()));
                }else{
                    places.put(tPlace.getPosition(),new Place(idCounter.getAndIncrement(), tPlace.getPosition(),tPlace.getName(),tPlace.getInterest()));
                }
            }catch (Exception e){
                logger.error("No Place",e);
            }
        }
        return places;
    }
    public synchronized ScheduledExecutorService getPlaceExecutor() {
        return placeExecutor;
    }
    public void setPlaceExecutor(ScheduledExecutorService placeExecutor) {
        this.placeExecutor = placeExecutor;
    }

}
