package com.dschulz.datagenerator.Visitor.Transmitter;

import com.dschulz.datagenerator.Const;
import com.dschulz.datagenerator.Math.Functions;
import com.dschulz.datagenerator.Place.Place;
import com.dschulz.datagenerator.Place.PlaceFactory;
import com.dschulz.datagenerator.SampleData;
import com.dschulz.datagenerator.TestClass;
import com.graphhopper.util.shapes.GHPoint;
import org.junit.Before;
import org.junit.Test;

import java.util.Map;
import java.util.TreeMap;

import static org.junit.Assert.*;

/**
 * Created by daniel on 08.08.16.
 */
public class TransmitterStationFactoryTest extends TestClass {

    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    public void createStations() throws Exception {
        assertTrue(TransmitterStationFactory.getInstance().getStations().size()==0);
        TransmitterStationFactory.getInstance().createStations();
        assertTrue(TransmitterStationFactory.getInstance().getStations().size()!=0);
    }

    @Test
    public void getTransmittersInRadius() throws Exception {
        TransmitterStationFactory.getInstance().createStations(0.5);
        for(Place place:SampleData.PLACES().values()) {
            TreeMap<Double, TransmitterStation> result = TransmitterStationFactory.getInstance().getTransmittersInRadius(place.getPosition(), 0.6);
            for(Map.Entry<Double,TransmitterStation> entry: result.entrySet()){
                assertTrue(entry.getKey()== Functions.Distance(entry.getValue().getPosition(),place.getPosition()));
                assertTrue(entry.getKey()<= Const.TRANSMITTER_MAX_RADIUS_IN_METER);
            }
            int size=result.size();
            assertTrue(size>=78);
            assertTrue(size<=82);
        }
    }

}