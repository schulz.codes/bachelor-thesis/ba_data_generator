package com.dschulz.datagenerator.Math;

import com.graphhopper.util.shapes.GHPoint;
import com.dschulz.datagenerator.Const;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;
import org.apache.commons.math3.geometry.euclidean.twod.Vector2D;

import java.util.Vector;

import static java.lang.Math.*;

/**
 * Created by daniel on 16.04.16.
 */
public class Functions {

    public static double DegreeBearing(GHPoint from, GHPoint to){
        return DegreeBearing(from.getLat(),from.getLon(),to.getLat(),to.getLon());
    }

    //Returns BearingIn Radians
    public static double DegreeBearing(
            double lat1, double lon1,
            double lat2, double lon2)
    {
        double dLon = toRadians(lon2-lon1);
        double dPhi = log(
                tan(toRadians(lat2)/2+PI/4)/tan(toRadians(lat1)/2+PI/4));
        if (abs(dLon) > PI)
            dLon = dLon > 0 ? -(2*PI-dLon) : (2*PI+dLon);
        return atan2(dLon, dPhi);
    }


    /**
     *
     * @param from
     * @param to
     * @return Distance in KM
     */
    public static double Distance(GHPoint from, GHPoint to) {
        double lat1 = toRadians(from.getLat());
        double lat2 = toRadians(to.getLat());
        double deltaLat = toRadians(to.getLat() - from.getLat());
        double deltaLon = toRadians(to.getLon() - from.getLon());

        double a = sin(deltaLat / 2) * sin(deltaLat / 2) +
                cos(lat1) * cos(lat2) *
                        sin(deltaLon / 2) * sin(deltaLon / 2);
        double c = 2 * atan2(sqrt(a), sqrt(1 - a));

        double d = Const.EARTH_RADIUS * c;
        return d;
    }

    public static GHPoint MoveDistanceInDirection(GHPoint startPoint, double bearing, double distanceInKilometer) {
        distanceInKilometer = distanceInKilometer/ Const.EARTH_RADIUS;

        double vLat1 = toRadians(startPoint.getLat());
        double vLng1 = toRadians(startPoint.getLon());

        double NewLat = asin(sin(vLat1) * cos(distanceInKilometer) +
                cos(vLat1) * sin(distanceInKilometer) * cos(bearing));

        double NewLng = vLng1 + atan2(sin(bearing) * sin(distanceInKilometer) * cos(vLat1),
                cos(distanceInKilometer) - sin(vLat1) * sin(NewLat));

        GHPoint newPoint=new GHPoint(toDegrees(NewLat), toDegrees(NewLng));
        return newPoint;
    }


}
