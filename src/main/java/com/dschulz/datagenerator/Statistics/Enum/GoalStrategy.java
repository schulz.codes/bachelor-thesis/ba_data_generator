package com.dschulz.datagenerator.Statistics.Enum;

/**
 * Created by daniel on 21.07.16.
 */
public enum GoalStrategy {
    NO_INTEREST_TIME, ALL_SEEN_TIME, FOUND_GOAL_TIME
}
