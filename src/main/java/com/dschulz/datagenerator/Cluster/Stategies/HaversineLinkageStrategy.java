package com.dschulz.datagenerator.Cluster.Stategies;

import com.dschulz.datagenerator.Cluster.Cluster;
import com.dschulz.datagenerator.Cluster.Distance;
import com.dschulz.datagenerator.Math.Functions;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Collection;

/**
 * Created by daniel on 21.07.16.
 */
public class HaversineLinkageStrategy implements  LinkageStrategy {
    @Override
    public Distance calculateDistance(Collection<Distance> distances) {
       throw new NotImplementedException();
    }

    @Override
    public Distance calculateDistance(Cluster fromCluster, Cluster toCluster) {
        return new Distance(Functions.Distance(fromCluster.getPosition(),toCluster.getPosition()));
    }
}
