package com.dschulz.datagenerator.Visitor.BehaviourStrategies.GoalStrategy;

import com.dschulz.datagenerator.Cluster.DirectedEdge;
import com.dschulz.datagenerator.Place.Place;
import com.dschulz.datagenerator.Place.PlaceFactory;
import com.dschulz.datagenerator.Routing.Hopper;
import com.dschulz.datagenerator.Statistics.Enum.GoalStrategy;
import com.dschulz.datagenerator.Statistics.Statistics;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.dschulz.datagenerator.Visitor.VisitorFactory;
import com.graphhopper.GHResponse;
import com.graphhopper.PathWrapper;
import com.graphhopper.util.shapes.GHPoint;

import java.util.PriorityQueue;
import java.util.Set;

/**
 * Created by daniel on 22.07.16.
 */
public class nearestPlacesFirstGoalStrategy implements VisitorGoalStrategy {

    Place lastPlace=null;

    @Override
    public GHPoint findGoal(Visitor visitor) {
        long startTime=System.nanoTime();
        Double minDist = Double.MAX_VALUE;
        Place nearestPlace = null;
        DirectedEdge minEdge= null;
        if (visitor.getInterestNeeded() <= visitor.getInterestReducePerTime()) {
            long endTime=System.nanoTime()-startTime;
            Statistics.getInstance().addGoalStrategyTime(this.getClass().getSimpleName(), GoalStrategy.NO_INTEREST_TIME,endTime);
            return visitor.getHome();
        } else {
            if (lastPlace == null) {
                for (Place place : PlaceFactory.getInstance().getPlaces().values()) {
                    GHResponse route = visitor.getRoutingStrategy().route(visitor.getPosition(), place.getPosition());
                    if (route.getBest().getDistance() < minDist) {
                        minDist = route.getBest().getDistance();
                        nearestPlace = place;

                    }
                }
                if(nearestPlace==null){
                    throw new NullPointerException();
                }
            } else {
                Set<DirectedEdge> edges = PlaceFactory.getInstance().getGraph().edgesOf(lastPlace);
                if (visitor.getVisitedPlaces().size() == PlaceFactory.getInstance().getPlaces().size()) {
                    long endTime = System.nanoTime() - startTime;
                    Statistics.getInstance().addGoalStrategyTime(this.getClass().getSimpleName(), GoalStrategy.ALL_SEEN_TIME, endTime);
                    return visitor.getHome();
                } else {
                    for (DirectedEdge edge : edges) {
                        if ((edge.getFrom() == lastPlace)) {
                            if(!visitor.getVisitedPlaces().contains(edge.getTo())) {
                                if(edge.getWeight() < minDist) {
                                    minDist = edge.getWeight();
                                    nearestPlace = edge.getTo();
                                    minEdge = edge;
                                }
                            }
                        }else{
                            if(!visitor.getVisitedPlaces().contains(edge.getFrom())) {
                                if(edge.getWeight() < minDist) {
                                    minDist = edge.getWeight();
                                    nearestPlace = edge.getFrom();
                                    minEdge = edge;
                                }
                            }
                        }
                    }
                    if(nearestPlace==null){
                        throw new NullPointerException();
                    }
                }
            }
        }
        long endTime = System.nanoTime() - startTime;
        Statistics.getInstance().addGoalStrategyTime(this.getClass().getSimpleName(), GoalStrategy.FOUND_GOAL_TIME, endTime);
        lastPlace = nearestPlace;
        return nearestPlace.getPosition();
    }

    @Override
    public String getName() {
        return "NPF";
    }
}
