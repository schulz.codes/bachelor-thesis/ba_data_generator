package com.dschulz.datagenerator.Visitor.BehaviourStrategies.EnjoyingStrategy;

import com.dschulz.datagenerator.Visitor.Visitor;
import com.graphhopper.util.shapes.GHPoint;

/**
 * Created by daniel on 19.04.16.
 */
public interface VisitorEnjoyStrategy {
    boolean leave(Visitor visitor, GHPoint place);

    double getInterestReduce(Visitor visitor, GHPoint place);

    String getName();
}
