package com.dschulz.datagenerator.Visitor.BehaviourStrategies.EnjoyingStrategy;

import com.dschulz.datagenerator.Place.Place;
import com.dschulz.datagenerator.Place.PlaceFactory;
import com.dschulz.datagenerator.SampleData;
import com.dschulz.datagenerator.Statistics.Enum.EnjoyingStrategy;
import com.dschulz.datagenerator.TestClass;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.dschulz.datagenerator.Visitor.VisitorFactory;
import com.graphhopper.util.shapes.GHPoint;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by daniel on 08.05.16.
 */
public class DefaultEnjoyingStrategyTest extends TestClass {
    public static GHPoint centerPoint = new GHPoint(50.682903, 10.937857);

    @Before
    public void setUp() throws Exception{
        super.setUp();
    }

    /*@Test
    public void testLeave() throws Exception {
        List<Visitor> visitorList=VisitorFactory.getInstance().InitVisitors(centerPoint,2,1);
        Visitor visitor = visitorList.get(0);
        visitor.setEnjoyingStrategy(new DefaultEnjoyingStrategy());
        double time=0;
        Place place=null;
        boolean wasWalking=(visitor.isEnjoying()==false);
        boolean wasEnjoying=(visitor.isEnjoying()==true);
        while(VisitorFactory.getInstance().getActiveVisitorsCount()>0) {
            visitor.run();
            if(time==0 && wasEnjoying==true){
                assertTrue(wasEnjoying);
                assertFalse(visitor.isEnjoying());
            }
            if(visitor.isEnjoying()==true && wasWalking) {
                place = PlaceFactory.getInstance().getPlaceToPosition(visitor.getPosition());
                double subjectiveInterest = place.getInterestFactor() * visitor.getPlaceAttachement();
                if (subjectiveInterest >= visitor.getInterestNeeded()) {
                    time = Math.ceil((visitor.getInterestNeeded() + visitor.getInterestReducePerTime()) / (visitor.getInterestReducePerTime()));
                } else {
                    time = Math.ceil(subjectiveInterest / visitor.getInterestReducePerTime());
                }
            }
            if(visitor.isEnjoying()){
                time--;
            }
            wasWalking=(visitor.isEnjoying()==false);
            wasEnjoying=(visitor.isEnjoying()==true);
        }
    }*/



    @Test
    public void testGetInterestReduce() throws Exception {
        List<Visitor> visitorList=VisitorFactory.getInstance().InitVisitors(centerPoint,2,1);
        Visitor visitor = visitorList.get(0);
        VisitorEnjoyStrategy enjStrat=new DefaultEnjoyingStrategy();
        List<Place> placeList=new ArrayList<>(SampleData.PLACES().values());
        assertTrue(visitor.getInterestReducePerTime()==enjStrat.getInterestReduce(visitor,placeList.get(0).getPosition()));
        while(VisitorFactory.getInstance().getActiveVisitorsCount()>0){
            visitor.run();
            assertTrue(visitor.getInterestReducePerTime()==enjStrat.getInterestReduce(visitor,placeList.get(0).getPosition()));
        }
    }

    @Test
    public void testGetName() throws Exception {

        assertTrue(new DefaultEnjoyingStrategy().getName().compareTo("DefaultEnjoyingStrategy")==0);
    }
}