package com.dschulz.datagenerator.Visitor.Transmitter.TransmitterDistributionStrategies;

import com.dschulz.datagenerator.Const;
import com.dschulz.datagenerator.Math.Functions;
import com.dschulz.datagenerator.Math.MathDistributions;
import com.dschulz.datagenerator.Place.PlaceFactory;
import com.dschulz.datagenerator.Statistics.Enum.TransmitterDistributionStrategy;
import com.dschulz.datagenerator.Statistics.Statistics;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStation;
import com.graphhopper.util.shapes.GHPoint;
import com.dschulz.datagenerator.Cluster.Cluster;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel on 15.06.16.
 */
public class RandomDistributionStrategy implements TransmitterStationDistributionStrategy {
    @Override
    public List<TransmitterStation> createStations(double fillFactor) {
        long startTime=System.nanoTime();
        Cluster cluster= PlaceFactory.getInstance().getClustered();
        double areaToFill= Math.PI*Math.pow(cluster.getDistanceValue(),2);
        double areaFilled=0;
        ArrayList<TransmitterStation> stations= new ArrayList<>();
        while(areaFilled<fillFactor*areaToFill) {
            double angle = MathDistributions.EqualRandom(0,1) * Math.PI * 2;
            //GHPoint pos =Functions.MoveDistanceInDirection(cluster.getPosition(),angle,(cluster.getDistanceValue()/1000)*MathDistributions.EqualRandom(0,1));
            GHPoint pos= MathDistributions.EqualInCircle(cluster.getPosition(),cluster.getDistanceValue()/1000);

            double stationRadius = MathDistributions.EqualRandom(Const.TRANSMITTER_MIN_RADIUS_IN_METER, Const.TRANSMITTER_MAX_RADIUS_IN_METER);
            areaFilled += Math.PI*Math.pow(stationRadius,2);
            stations.add(new TransmitterStation(pos,stationRadius));
        }
        long endTime=System.nanoTime()-startTime;
        Statistics.getInstance().addTransmitterDistributionStrategyTime(this.getClass().getSimpleName(), TransmitterDistributionStrategy.TIME,endTime);
        return stations;
    }
}
