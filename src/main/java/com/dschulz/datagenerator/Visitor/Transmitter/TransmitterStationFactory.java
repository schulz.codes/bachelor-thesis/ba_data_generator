package com.dschulz.datagenerator.Visitor.Transmitter;

import com.dschulz.datagenerator.Const;
import com.dschulz.datagenerator.Math.Functions;
import com.dschulz.datagenerator.Visitor.Provider.DataProvider;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterDistributionStrategies.TransmitterStationDistributionStrategy;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.graphhopper.util.shapes.GHPoint;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.*;

/**
 * Created by daniel on 14.06.16.
 */
public class TransmitterStationFactory implements TransmitterStationDistributionStrategy {

    private static TransmitterStationFactory instance = new TransmitterStationFactory();

    private TransmitterStationFactory() {
        try {
            creationStrategy= Const.TRANSMITTER_STATION_DISTRIBUTION_STRATEGY.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
    public static synchronized TransmitterStationFactory getInstance(){
        if(instance==null){
            instance=new TransmitterStationFactory();
        }
        return instance;
    }
    //endregion

    private TransmitterStationDistributionStrategy creationStrategy;
    private List<TransmitterStation> stations = Collections.synchronizedList(new ArrayList<TransmitterStation>());

    public List<TransmitterStation> getStations() {
        return stations;
    }

    public List<TransmitterStation> createStations(){
        return createStations(Const.TRANSMITTER_FILL_FACTOR);
    }

    public List<TransmitterStation> createStations(double fillFactor){
        stations=creationStrategy.createStations(fillFactor);
        DataProvider prov = null;
        try {
            prov = Const.VISITOR_DATA_PROVIDER.newInstance();
            prov.createTransmitterStations(stations);
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch(NotImplementedException e){
            e.printStackTrace();
        }
        return stations;
    }

    public TreeMap<Double, TransmitterStation> getTransmittersInMaxRadius(Visitor visitor){
        double radInKM=Const.TRANSMITTER_MAX_RADIUS_IN_METER/1000;
        return getTransmittersInRadius(visitor.getPosition(),radInKM);
    }

    /**
     *
     * @param position
     * @param radius in KM
     * @return
     */
    public TreeMap<Double, TransmitterStation> getTransmittersInRadius(GHPoint position, double radius){
        TreeMap<Double,TransmitterStation> stationHashMap = new TreeMap<>();
        Iterator<TransmitterStation> stationIt=TransmitterStationFactory.getInstance().getStations().iterator();
        while(stationIt.hasNext()){
            TransmitterStation station= stationIt.next();
            double distance= Functions.Distance(station.getPosition(),position);
            if(distance<=radius){
                stationHashMap.put(distance,station);
            }
        }
        return stationHashMap;
    }

}
