package com.dschulz.datagenerator.Statistics.Enum;

/**
 * Created by daniel on 21.07.16.
 */
public enum Cluster {
    WEIGHTED_CLUSTERING_TIME, CREATE_LINKAGES_TIME, CLUSTERING_TIME, CREATE_CLUSTER_TIME, CREATE_WEIGHTED_CLUSTER_TIME
}
