package com.dschulz.datagenerator.Visitor;

import com.dschulz.datagenerator.Math.Functions;
import com.dschulz.datagenerator.TestClass;
import com.graphhopper.util.shapes.GHPoint;
import com.dschulz.datagenerator.Visitor.BehaviourStrategies.EnjoyingStrategy.VisitorEnjoyStrategy;
import com.dschulz.datagenerator.Visitor.BehaviourStrategies.GoalStrategy.VisitorGoalStrategy;
import com.dschulz.datagenerator.Visitor.BehaviourStrategies.RoutingStrategy.VisitorRoutingStrategy;
import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.reflections.Reflections;

import java.util.*;
import java.util.concurrent.*;

import static org.junit.Assert.*;

public class VisitorTest extends TestClass {
    private static final Logger logger = Logger.getLogger("Visitor");
    public static GHPoint centerPoint = new GHPoint(50.682903, 10.937857);


    @Before
    public void setUp() throws Exception {
        super.setUp();
    }



    @Test
    public void testGetPositionMap() throws Exception {

        List<Visitor> visitorList = VisitorFactory.getInstance().InitVisitors(centerPoint, 2, 1);
        Visitor visitor = visitorList.get(0);
        assertEquals(0,visitor.getPositionMap().size());
        visitor.run();
        Long maxKey = Collections.max(visitor.getPositionMap().keySet());
        Long minKey = Collections.min(visitor.getPositionMap().keySet());
        GHPoint maxPoint = visitor.getPositionMap().get(maxKey);
        GHPoint minPoint = visitor.getPositionMap().get(minKey);
        assertEquals(minPoint.getLat(), visitor.getHome().getLat(),0.0001);
        assertEquals(minPoint.getLon(),visitor.getHome().getLon(),0.0001);
        assertEquals(maxPoint.getLat(), visitor.getHome().getLat(), 0.0001);
        assertEquals(maxPoint.getLon(), visitor.getHome().getLon(), 0.0001);
    }

    @Test
    public void testGetTimingId() throws Exception {

        List<Visitor> visitorList = VisitorFactory.getInstance().InitVisitors(centerPoint, 2, 1);
        Visitor visitor = visitorList.get(0);
        long prevTimingId = 0;
        while (VisitorFactory.getInstance().getActiveVisitorsCount() > 0) {
            visitor.run();
            assertTrue(visitor.getTimingId() > prevTimingId);
            prevTimingId = visitor.getTimingId();
        }
    }

    @Test

    public void testGetHome() throws Exception {

        List<Visitor> visitorList = VisitorFactory.getInstance().InitVisitors(centerPoint, 0, 1);
        Visitor visitor = visitorList.get(0);
        visitor.getHome().equals(centerPoint);
    }

    @Test

    public void testGetPosition() throws Exception {

        List<Visitor> visitorList = VisitorFactory.getInstance().InitVisitors(centerPoint, 2, 1);
        while (VisitorFactory.getInstance().getActiveVisitorsCount() > 0) {
            try {
                List<Visitor> newActive = VisitorFactory.getInstance().getActiveVisitorList();
                Iterator<Visitor> it = newActive.iterator();
                while (it.hasNext()) {
                    Visitor curVisitor = it.next();
                    Long doSimStart = System.nanoTime();
                    curVisitor.run();
                    assertNotNull(curVisitor.getPosition());
                    //Statistics.getInstance().addVisitorDoSimulationTime(System.nanoTime() - doSimStart);
                }
            } catch (Exception e) {
                //logger.error("listSemaphore", e);
            }

        }
    }

    @Test
    public void testGetInterestNeeded() throws Exception {

        List<Visitor> visitorList = VisitorFactory.getInstance().InitVisitors(centerPoint, 2, 1);
        Visitor visitor = visitorList.get(0);
        double prevInterestNeeded = Double.POSITIVE_INFINITY;
        while(VisitorFactory.getInstance().getActiveVisitorsCount()>=1){
            visitor.run();
            assertTrue(visitor.getInterestNeeded() <= prevInterestNeeded);
            prevInterestNeeded = visitor.getInterestNeeded();
        }
    }

    @Test

    public void testGetPlaceAttachement() throws Exception {

        List<Visitor> visitorList = VisitorFactory.getInstance().InitVisitors(centerPoint, 2, 1);
        Visitor visitor = visitorList.get(0);
        while(VisitorFactory.getInstance().getActiveVisitorsCount()>=1){
            visitor.run();
            if (visitor.isEnjoying()) {
                if(visitor.getPlaceAttachement()<0){
                    assertEquals(-1, visitor.getPlaceAttachement(), 0);//This is for the enjoyingStrategy to notice that it has to calculate a new Attachement
                }else {
                    assertTrue(visitor.getPlaceAttachement()> 0);
                }
            } else {
                assertEquals(-1, visitor.getPlaceAttachement(), 0);
            }
        }

    }

    @Test

    public void testGetInterestReducePerTime() throws Exception {

        List<Visitor> visitorList = VisitorFactory.getInstance().InitVisitors(centerPoint, 2, 1);
        Visitor visitor = visitorList.get(0);
        while(VisitorFactory.getInstance().getActiveVisitorsCount()>=1){
            visitor.run();
            assertTrue(visitor.getInterestReducePerTime() > 0);
        }
    }

    @Test

    public void testGetId() throws Exception {

        int simultanousCalls = 2000;

        final CyclicBarrier gate = new CyclicBarrier(simultanousCalls + 1);
        ArrayList<Callable<List<Visitor>>> list = new ArrayList<Callable<List<Visitor>>>();
        ExecutorService executorService = Executors.newFixedThreadPool(simultanousCalls);
        ArrayList<Future<List<Visitor>>> futures = new ArrayList<>();
        for (int i = 0; i < simultanousCalls; i++) {
            list.add(new Callable<List<Visitor>>() {
                @Override
                public List<Visitor> call() throws Exception {
                    gate.await();
                    return VisitorFactory.getInstance().InitVisitors(centerPoint, 2, 1);
                }

            });
            futures.add(executorService.submit(list.get(list.size() - 1)));
        }
        gate.await();
        HashSet<Long> seenIds = new HashSet<>();
        assertEquals(simultanousCalls, futures.size());
        for (Future<List<Visitor>> future : futures) {
            List<Visitor> futureList = future.get();
            for (Visitor visitor : futureList) {
                long vId = visitor.getId();
                assertNotNull(vId);
                assertFalse(seenIds.contains(vId));
                seenIds.add(vId);
            }
        }
    }


    @Test

    public void testGetEnjoyingStrategy() throws Exception {

        List<Visitor> visitorList = VisitorFactory.getInstance().InitVisitors(centerPoint, 2, 5);
        for (Visitor visitor : visitorList) {
            assertTrue(visitor.getEnjoyingStrategy() instanceof VisitorEnjoyStrategy);
        }
    }

    @Test

    public void testGetRoutingStrategy() throws Exception {

        List<Visitor> visitorList = VisitorFactory.getInstance().InitVisitors(centerPoint, 2, 5);
        for (Visitor visitor : visitorList) {
            assertTrue(visitor.getRoutingStrategy() instanceof VisitorRoutingStrategy);
        }
    }

    @Test

    public void testGetGoalStrategy() throws Exception {

        List<Visitor> visitorList = VisitorFactory.getInstance().InitVisitors(centerPoint, 2, 5);
        for (Visitor visitor : visitorList) {
            assertTrue(visitor.getGoalStrategy() instanceof VisitorGoalStrategy);
        }
    }

}