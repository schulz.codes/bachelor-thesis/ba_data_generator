package com.dschulz.datagenerator.Visitor.Transmitter.TransmitterDistributionStrategies;

import com.dschulz.datagenerator.Const;
import com.dschulz.datagenerator.Math.Functions;
import com.dschulz.datagenerator.Math.MathDistributions;
import com.dschulz.datagenerator.Place.Place;
import com.dschulz.datagenerator.Place.PlaceFactory;
import com.dschulz.datagenerator.Statistics.Enum.TransmitterDistributionStrategy;
import com.dschulz.datagenerator.Statistics.Statistics;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStation;
import com.graphhopper.util.shapes.GHPoint;
import com.dschulz.datagenerator.Cluster.Cluster;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by daniel on 16.06.16.
 */
public class CircleDistributionStrategy implements TransmitterStationDistributionStrategy {
    private static final Logger logger = Logger.getLogger("Visitor");
    @Override
    public List<TransmitterStation> createStations(double fillFactor) {
        logger.info("Creating Transmitter Stations");
        long startTime=System.nanoTime();
        Cluster mainCluster= PlaceFactory.getInstance().getClustered();
        Double maxDist=0.0;
        for(Place place:PlaceFactory.getInstance().getPlaces().values()){
            Double dist=Functions.Distance(mainCluster.getPosition(),place.getPosition());
            if(dist>maxDist){
                maxDist=dist;
            }
        }
        Double clusterRadius=maxDist+mainCluster.getDistanceValue();
        List<TransmitterStation> stations= new ArrayList<>();
        double distancePerRound=Const.TRANSMITTER_MIN_RADIUS_IN_METER/fillFactor;
        int rounds=1;
        double roundPerimeter=0;
        while((distancePerRound*rounds)<clusterRadius){
            roundPerimeter=2*Math.PI*rounds*distancePerRound;
            double roundSteps=(roundPerimeter/distancePerRound);
            for(int i=0; i<=roundSteps;i++){
                double angle = i*Math.PI*2/roundSteps;
                GHPoint stationPos=Functions.MoveDistanceInDirection(mainCluster.getPosition(),angle,(distancePerRound*rounds)/1000);
                double stationRadius = MathDistributions.EqualRandom(Const.TRANSMITTER_MIN_RADIUS_IN_METER, Const.TRANSMITTER_MAX_RADIUS_IN_METER);
                stations.add(new TransmitterStation(stationPos,stationRadius));
            }
            rounds++;
        }
        long endTime=System.nanoTime()-startTime;
        logger.info("Creating Transmitter Stations took "+endTime+" ns");
        Statistics.getInstance().addTransmitterDistributionStrategyTime(this.getClass().getSimpleName(), TransmitterDistributionStrategy.TIME,endTime);
        return stations;
    }
}
