package com.dschulz.datagenerator.Visitor.Transmitter.TransmitterDistributionStrategies;

import com.dschulz.datagenerator.TestClass;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStation;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStationFactory;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by daniel on 08.08.16.
 */
public class CircleDistributionStrategyTest extends TestClass {
    @Before
    public void setUp() throws Exception{
        super.setUp();
    }
    @Test
    public void createStations() throws Exception {
        List<TransmitterStation> stations= TransmitterStationFactory.getInstance().createStations(0.5);
        int sizeA=stations.size();
        assertTrue(sizeA==761);
        stations= TransmitterStationFactory.getInstance().createStations(0.75);
        int sizeB=stations.size();
        assertTrue(sizeB==1600);
        stations= TransmitterStationFactory.getInstance().createStations(1);
        int sizeC=stations.size();
        assertTrue(sizeC==2936);

    }

}