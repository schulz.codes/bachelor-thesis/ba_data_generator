package com.dschulz.datagenerator.Cluster;

import com.dschulz.datagenerator.Place.Place;
import com.dschulz.datagenerator.Routing.Hopper;
import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.PathWrapper;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.util.Locale;

/**
 * Created by daniel on 09.04.16.
 */
public class DirectedEdge extends DefaultWeightedEdge{
    Place From;
    Place To;
    PathWrapper Path;
    public DirectedEdge(Place from, Place to){
        From= from;
        To= to;
        // simple configuration of the request object, see the GraphHopperServlet classs for more possibilities.
        GHRequest req = new GHRequest(from.getPosition(),to.getPosition()).
                setWeighting("fastest").
                setVehicle("foot").
                setLocale(Locale.GERMAN);
        GHResponse rsp = Hopper.getInstance().route(req);

// first check for errors
        if(rsp.hasErrors()) {
            // handle them!
            // rsp.getErrors()
            return;
        }

// use the best path, see the GHResponse class for more possibilities.
        Path = rsp.getBest();
    }

    public double getWeight(){
        return Path.getDistance();
    }

    public Place getFrom() {
        return From;
    }

    public Place getTo() {
        return To;
    }

    public PathWrapper getPath() {
        return Path;
    }

}
