/*******************************************************************************
 * Copyright 2013 Lars Behnke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.dschulz.datagenerator;

import com.dschulz.datagenerator.Cluster.Algorithm.ClusteringAlgorithm;
import com.dschulz.datagenerator.Cluster.Algorithm.GraphClusteringAlgorithm;
import com.dschulz.datagenerator.Cluster.Cluster;
import com.dschulz.datagenerator.Cluster.DirectedEdge;
import com.dschulz.datagenerator.Cluster.Stategies.CalculatedLinkageStrategy;
import com.dschulz.datagenerator.Place.Place;
import com.dschulz.datagenerator.Place.PlaceFactory;
import com.dschulz.datagenerator.Place.PlaceJson;
import com.graphhopper.util.shapes.GHPoint;
import org.jgrapht.WeightedGraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public final class SampleData {
  private static WeightedGraph<Place, DirectedEdge> G=null;
  private static HashMap<GHPoint, Place> PLACES=null;
  private static List<PlaceJson> PLACEJSON=null;
  private static Cluster CLUSTER=null;

  public static final List<PlaceJson> PLACEJSON(){
    if(PLACEJSON==null){
      PLACEJSON=PlaceFactory.getInstance().parsePlaceJSON("./src/test/resources/places.json");
    }
    return PLACEJSON;
  }
  public static final HashMap<GHPoint, Place>  PLACES(){
    if(PLACES==null) {
      PLACES= PlaceFactory.getInstance().InitPlaces(PLACEJSON());
    }
    return PLACES;
  }
  public static final WeightedGraph<Place, DirectedEdge> GRAPH(){
    if(G==null) {
      SampleData.G = PlaceFactory.getInstance().makeGraph(new ArrayList<>(PLACES().values()));
    }
    return G;
  }
  public static final Cluster CLUSTER(){
    if(CLUSTER==null){
      ClusteringAlgorithm alg=new GraphClusteringAlgorithm();
      CLUSTER=alg.performClustering(GRAPH(),new CalculatedLinkageStrategy());
    }
    return CLUSTER;
  }

}
