package com.dschulz.datagenerator.Place;

import com.dschulz.datagenerator.App;
import com.dschulz.datagenerator.Cluster.DirectedEdge;
import com.dschulz.datagenerator.Routing.Hopper;
import com.dschulz.datagenerator.SampleData;
import com.dschulz.datagenerator.TestClass;
import com.dschulz.datagenerator.Visitor.VisitorFactory;
import com.graphhopper.util.shapes.GHPoint;
import org.apache.log4j.BasicConfigurator;
import org.jgrapht.WeightedGraph;
import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;

import static org.junit.Assert.*;

/**
 * Created by daniel on 08.08.16.
 */
public class PlaceFactoryTest extends TestClass{

    @Before
    public void setUp() throws Exception {
        BasicConfigurator.configure();
        App.propertiesExist(realDUMMYPropertiesFile);
        Hopper.getInstance().loadFromOSM(Hopper.testOSM, false, true);

        cluster= SampleData.CLUSTER();
        graph = SampleData.GRAPH();
        places= SampleData.PLACES();
        placejson= SampleData.PLACEJSON();


        Field pFInstance = PlaceFactory.class.getDeclaredField("instance");
        pFInstance.setAccessible(true);
        pFInstance.set(null, null);


        Field vFInstance= VisitorFactory.class.getDeclaredField("instance");
        vFInstance.setAccessible(true);
        vFInstance.set(null, null);
    }

    @Test
    public void getInstance() throws Exception {
        assertTrue(PlaceFactory.getInstance() instanceof  PlaceFactory);
    }

    @Test
    public void getPlaceToPosition() throws Exception {
        PlaceFactory.getInstance().InitPlaces(this.placejson);
        for (Map.Entry<GHPoint, Place> tPlaceEntry : PlaceFactory.getInstance().getPlaces().entrySet()) {
            Place place1=tPlaceEntry.getValue();
            Place place2= PlaceFactory.getInstance().getPlaceToPosition(tPlaceEntry.getKey());

            assertEquals(place1, place2);
        }
    }

    @Test
    public void initPlaces() throws Exception {
        assertTrue(PlaceFactory.getInstance().getPlaces().size()==0);
        PlaceFactory.getInstance().InitPlaces(this.placejson);
        assertTrue(PlaceFactory.getInstance().getPlaces().size()==7);
    }

    @Test
    public void parsePlaceJSON() throws Exception {
        List<PlaceJson> placeJsonList= PlaceFactory.getInstance().parsePlaceJSON("./src/test/resources/places.json");
        assertEquals(7,placeJsonList.size());
        assertEquals("Humboldtbau",placeJsonList.get(0).getName());
        assertEquals(50.682903,placeJsonList.get(0).getPosition().getLat(),0);
        assertEquals(10.937857,placeJsonList.get(0).getPosition().getLon(),0);
        assertEquals(50.68529,placeJsonList.get(4).getPosition().getLat(),0);
        assertEquals(10.94152,placeJsonList.get(4).getPosition().getLon(),0);
        assertNotNull(placeJsonList.get(0).getInterest());
    }

    @Test
    public void makeGraph() throws Exception {
        int placeCount = SampleData.PLACES().size();
        WeightedGraph<Place,DirectedEdge> graph= PlaceFactory.getInstance().makeGraph(new ArrayList<>(SampleData.PLACES().values()));
        assertEquals(placeCount, 7);
        assertEquals(graph.vertexSet().size(), placeCount);
        assertEquals(graph.edgeSet().size(), 0.5 * placeCount * (placeCount - 1), 0);
    }

}