package com.dschulz.datagenerator;


import com.dschulz.datagenerator.Place.Place;
import com.dschulz.datagenerator.Place.PlaceFactory;
import com.dschulz.datagenerator.Place.PlaceJson;
import com.dschulz.datagenerator.Routing.Hopper;
import com.dschulz.datagenerator.Statistics.Statistics;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStation;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStationFactory;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.dschulz.datagenerator.Visitor.VisitorFactory;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.dschulz.datagenerator.Cluster.Cluster;
import org.apache.commons.cli.*;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.reflections.Reflections;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.Type;
import java.util.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;


/**
 * Hello world!
 */
public class App {
    public static Logger logger = Logger.getLogger(App.class.getName());

    public static File propertiesFile=null;
    static List<PlaceJson> places=null;
    static long startTime;
    static long setupTime;



    public static void main(String[] args) {
        setupTime= System.currentTimeMillis();
        Logger.getLogger("org.mongodb.driver").setLevel(Level.FATAL);
        //Logger.getLogger("org.apache.http.wire").setLevel(Level.WARN);
        //Logger.getLogger("org.apache.http.headers").setLevel(Level.WARN);
        BasicConfigurator.configure();

        Options options = new Options();
        options.addOption("h","help",false,"zeigt diese hilfe an");
        options.addOption(Option.builder("i").argName("osm_src> <force:[true|false]")
                .longOpt("import")
                .hasArgs()
                .numberOfArgs(2)
                .desc("select the OSM-File which should be imported" )
                .build());
        options.addOption(Option.builder("run")//TODO check Arguments
                .argName("osm_src> <force> <[config_src <[places_json <[visitor_under_100]>]>]")
                .hasArgs()
                .numberOfArgs(5)
                .desc("run datagenerator with provided Config")
                .build());
        CommandLineParser parser = new DefaultParser();
        int visitor_under_100=0;
        try {
            CommandLine cmd = parser.parse( options, args);
            if (cmd.hasOption("help")) {
                HelpFormatter formatter = new HelpFormatter();
                formatter.printHelp( "App", options );
                return;
            }
            if(cmd.hasOption("i")){
                String[] values=cmd.getOptionValues("i");
                boolean forceDelete=false;
                if(new String("true").equals(values[1])){
                    forceDelete=true;
                }
                File file= new File(values[0]);
                if(!file.exists()) {
                    System.out.println("The provided File does not exist");
                    return;
                }
                Hopper.getInstance().loadFromOSM(values[0],forceDelete);
            }
            if(cmd.hasOption("run")){
                String[] values=cmd.getOptionValues("run");
                File file= new File(values[0]);
                boolean forceDelete=false;
                if(new String("true").equals(values[1])){
                    forceDelete=true;
                }
                if(!file.exists()) {
                    System.out.println("The provided File does not exist");
                    return;
                }
                Hopper.getInstance().loadFromOSM(values[0],forceDelete);
                if(values.length!=2){
                    propertiesFile = new File(values[2]);
                    if(values.length!=3){
                        places=PlaceFactory.getInstance().parsePlaceJSON(values[3]);
                        PlaceFactory.getInstance().setPlaceExecutor(Executors.newScheduledThreadPool(places.size()*2));
                        if(values.length!=4){
                            visitor_under_100=Integer.parseInt(values[4]);
                        }
                    }
                }
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }
        if(propertiesFile==null){
            propertiesFile=new File("config.properties");
        }
        if (propertiesFile.exists() && propertiesExist(propertiesFile)) {
            System.out.println("Properties file "+propertiesFile+" was loaded");
            if(visitor_under_100!=0){
                Const.VISITOR_COUNT_UNDER_100=visitor_under_100;
            }

        } else {
            System.out.println("Properties file is being created");
            createProperties(propertiesFile);
            System.out.println("Properties was created!");
        }
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            public void run() {
                Statistics.getInstance().displayStatistics();
            }
        }));
        logger.info("Command Evaluation and loading Properties took "+(System.currentTimeMillis()-setupTime));
        try {
            startSimulation(places);
        }catch(Exception ex){
            logger.error(ex);
        }


    }

    public static synchronized long getSimulationStart() {
        return startTime;
    }
    public static synchronized long getSetupTime() {
        return setupTime;
    }

    protected static void startSimulation(List<PlaceJson> places) throws MissingArgumentException {
        if(places==null) {
            throw new MissingArgumentException("There were no places defined please make shure your places.json is correct");
        }else{
            logger.info("Init Places");
            PlaceFactory.getInstance().InitPlaces(places);
        }
        if(Const.TRANSMITTER_STATIONS) {
            logger.info("Init TransmitterStations");
            TransmitterStationFactory.getInstance().createStations();
        }
        List<Cluster> tClustered = PlaceFactory.getInstance().getClustered().getAllChildren();
        setupTime=System.currentTimeMillis()-setupTime;
        logger.info("Initalizing Simulation took "+setupTime+" ms");
        startTime = System.currentTimeMillis();
        for (Cluster tClusterChilds : tClustered) {
            try {
                PlaceFactory.getInstance().getPlaceExecutor().scheduleAtFixedRate(tClusterChilds, 0, (long) Const.SIMULATION_CLUSTER_SLEEPTIME, TimeUnit.MILLISECONDS);
            }catch(Exception e){
                logger.error("placeExecutor",e);
            }
        }
        /*VisitorFactory instance=VisitorFactory.getInstance();
        if(Const.BATCH_PROCESSING) {
            while (Const.MAX_VISITORS_TOTAL > instance.getVisitorsCount() || instance.getActiveVisitorsCount() > 0) {
                try {
                    //logger.info("NEW APP SIMULATION ROUND___________________________");
                    List<Visitor> newActive = VisitorFactory.getInstance().getActiveVisitorList();
                    Iterator<Visitor> it = newActive.iterator();
                    while (it.hasNext()) {
                        Visitor curVisitor = it.next();
                        Long doSimStart = System.nanoTime();
                        curVisitor.DoSimulation();
                        Statistics.getInstance().addVisitorDoSimulationTime(curVisitor, com.dschulz.datagenerator.Statistics.Enum.Visitor.BATCH,System.nanoTime() - doSimStart);
                    }
                } catch (Exception e) {
                    logger.error("listSemaphore", e);
                }

            }
        }*/
    }

        public static boolean propertiesExist(File propertiesFile) {
        Properties prop = new Properties();
        InputStream input = null;
        boolean exists = false;

        try {
            input = new FileInputStream(propertiesFile);

            prop.load(input);

            exists = prop.getProperty("VISITOR_DATA_PROVIDER") != null
                    && prop.getProperty("UPDATE_DURING_SIMULATION") != null
                    && prop.getProperty("BATCH_PROCESSING") != null
                    && prop.getProperty("MIN_BUILDING_INTEREST") != null
                    && prop.getProperty("MAX_BUILDING_INTEREST") != null
                    && prop.getProperty("SIMULATION_CLUSTER_SLEEPTIME") != null
                    && prop.getProperty("SIMULATION_CLUSTER_INTEREST_THRESHOLD") != null
                    && prop.getProperty("SIMULATION_CLUSTER_PLACES_DISTANCE") != null
                    && prop.getProperty("VISITOR_SLEEPTIME") != null
                    && prop.getProperty("MAX_VISITORS_SAME_TIME") != null
                    && prop.getProperty("MAX_VISITORS_TOTAL") != null
                    && prop.getProperty("MIN_INTEREST_NEED") != null
                    && prop.getProperty("MAX_INTEREST_NEED") != null
                    && prop.getProperty("MIN_INTEREST_REDUCE_PER_TIME") != null
                    && prop.getProperty("MAX_INTEREST_REDUCE_PER_TIME") != null
                    && prop.getProperty("MONGO_COLLECTION_VISITOR") != null
                    && prop.getProperty("MONGO_DB_NAME") != null
                    && prop.getProperty("MONGO_ADDRESS") != null
                    && prop.getProperty("MONGO_PORT") != null
                    && prop.getProperty("HTTP_PROVIDER_FINALIZE_URL") != null
                    && prop.getProperty("HTTP_PROVIDER_UPDATE_URL") != null
                    && prop.getProperty("HTTP_PROVIDER_CREATE_URL") != null;

            for (Map.Entry<Object, Object> e : prop.entrySet()) {
                Field field = Const.class.getDeclaredField(e.getKey().toString());
                String tConstName = ((Class) field.getType()).getName();
                //System.out.println(tConstName);
                switch (tConstName) {
                    case "boolean":
                        field.setBoolean(null, Boolean.parseBoolean(e.getValue().toString()));
                        break;
                    case "double":
                        field.setDouble(null, Double.parseDouble(e.getValue().toString()));
                        break;
                    case "java.lang.String":
                        field.set(null, e.getValue().toString());
                        break;
                    case "int":
                        field.set(null, Integer.parseInt(e.getValue().toString()));
                        break;
                    case "java.lang.Class":
                        field.set(null, Class.forName(e.getValue().toString()));
                        break;
                }
            }
            Field field = Const.class.getDeclaredField("PROPERTY_FILE_NAME");
            field.set(null, propertiesFile.getName());



        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return exists;
    }

    protected static void createProperties(File propertiesFile) {
        Properties prop = new Properties();
        OutputStream output;
        output = null;

        try {

            output = new FileOutputStream(propertiesFile);

            prop.setProperty("VISITOR_DATA_PROVIDER", Const.VISITOR_DATA_PROVIDER.getName());
            prop.setProperty("UPDATE_DURING_SIMULATION", String.valueOf(Const.UPDATE_DURING_SIMULATION));
            prop.setProperty("BATCH_PROCESSING", String.valueOf(Const.BATCH_PROCESSING));
            prop.setProperty("MIN_BUILDING_INTEREST", String.valueOf(Const.MIN_BUILDING_INTEREST));
            prop.setProperty("MAX_BUILDING_INTEREST", String.valueOf(Const.MAX_BUILDING_INTEREST));
            prop.setProperty("SIMULATION_CLUSTER_SLEEPTIME", String.valueOf(Const.SIMULATION_CLUSTER_SLEEPTIME));
            prop.setProperty("SIMULATION_CLUSTER_INTEREST_THRESHOLD", String.valueOf(Const.SIMULATION_CLUSTER_INTEREST_THRESHOLD));
            prop.setProperty("SIMULATION_CLUSTER_PLACES_DISTANCE", String.valueOf(Const.SIMULATION_CLUSTER_PLACES_DISTANCE));
            prop.setProperty("VISITOR_SLEEPTIME", String.valueOf(Const.VISITOR_SLEEPTIME));
            prop.setProperty("MAX_VISITORS_SAME_TIME", String.valueOf(Const.MAX_VISITORS_SAME_TIME));
            prop.setProperty("MAX_VISITORS_TOTAL", String.valueOf(Const.MAX_VISITORS_TOTAL));
            prop.setProperty("MIN_INTEREST_NEED", String.valueOf(Const.MIN_INTEREST_NEED));
            prop.setProperty("MAX_INTEREST_NEED", String.valueOf(Const.MAX_INTEREST_NEED));
            prop.setProperty("MIN_INTEREST_REDUCE_PER_TIME", String.valueOf(Const.MIN_INTEREST_REDUCE_PER_TIME));
            prop.setProperty("MAX_INTEREST_REDUCE_PER_TIME", String.valueOf(Const.MAX_INTEREST_REDUCE_PER_TIME));
            prop.setProperty("MONGO_COLLECTION_VISITOR", Const.MONGO_COLLECTION_VISITOR);
            prop.setProperty("MONGO_DB_NAME", Const.MONGO_DB_NAME);
            prop.setProperty("MONGO_ADDRESS", Const.MONGO_ADDRESS);
            prop.setProperty("MONGO_PORT", String.valueOf(Const.MONGO_PORT));

            // save properties to project root folder
            prop.store(output, null);

        } catch (IOException io) {
            io.printStackTrace();
        } finally {
            if (output != null) {
                try {
                    output.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

        }
    }


}