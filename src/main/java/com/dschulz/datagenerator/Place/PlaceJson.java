package com.dschulz.datagenerator.Place;

import com.graphhopper.util.shapes.GHPoint;

/**
 * Created by daniel on 08.05.16.
 *
 * Class for reading Place-Description from JSON with Gson Library
 *
 * Is used in App.parsePlaceJSON and Place.InitPlaces(List<PlaceJson> places)
 * interest in JSON is an optional parameter.
 */
public class PlaceJson {
    String name;
    GHPoint position;
    Double interest=null;

    public PlaceJson(GHPoint point, String name, Double interest){
        this.position=point;
        this.name=name;
        this.interest=interest;
    }

    public Double getInterest() {
        return interest;
    }

    public String getName() {
        return name;
    }

    public GHPoint getPosition() {
        return position;
    }
}
