package com.dschulz.datagenerator.Visitor.BehaviourStrategies.RoutingStrategy;

import com.dschulz.datagenerator.Routing.Hopper;
import com.dschulz.datagenerator.Statistics.Enum.RoutingStrategy;
import com.dschulz.datagenerator.Statistics.Statistics;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import com.graphhopper.PathWrapper;
import com.graphhopper.util.shapes.GHPoint;

import java.util.HashMap;

/**
 * Created by daniel on 16.04.16.
 */
public class shortestRouteRoutingStrategy implements VisitorRoutingStrategy
{
    private static HashMap<String, GHResponse> calculatedRoutings = new HashMap<String, GHResponse>();

    @Override
    public PathWrapper getRoute(Visitor visitor, GHPoint nextGoal) {
        GHResponse tResp=this.route(visitor.getPosition(),nextGoal);
        PathWrapper tRespPath=tResp.getBest();
        return tRespPath;
    }

    @Override
    public GHResponse route(GHPoint from, GHPoint to) {
        long startTime=System.nanoTime();
        if(calculatedRoutings.containsKey(from.toString()+to.toString())){
            long endTime=System.nanoTime()-startTime;
            Statistics.getInstance().addRoutingStrategyTime(this.getClass().getSimpleName(), RoutingStrategy.FROM_CACHE_TIME,endTime);
            return calculatedRoutings.get(from.toString()+to.toString());
        }
        GHRequest tGHRequest = new GHRequest(from, to).setWeighting("shortest");
        GHResponse tResp;
        tResp= Hopper.getInstance().route(tGHRequest);
        long endTime=System.nanoTime()-startTime;
        Statistics.getInstance().addRoutingStrategyTime(this.getClass().getSimpleName(),RoutingStrategy.CALCULATED_TIME,endTime);
        calculatedRoutings.put(from.toString()+to.toString(),tResp);
        return tResp;
    }
    @Override
    public String getName() {
        return "SR";
    }

}
