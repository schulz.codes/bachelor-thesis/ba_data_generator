package com.dschulz.datagenerator.Cluster.Algorithm;

import com.dschulz.datagenerator.Cluster.*;
import com.dschulz.datagenerator.Cluster.Cluster;
import com.dschulz.datagenerator.Cluster.Stategies.LinkageStrategy;
import com.dschulz.datagenerator.Cluster.DirectedEdge;
import com.dschulz.datagenerator.Place.Place;
import com.dschulz.datagenerator.Statistics.Statistics;
import org.apache.log4j.Logger;
import org.jgrapht.WeightedGraph;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.*;

public class GraphClusteringAlgorithm implements ClusteringAlgorithm {
    static final Logger logger = Logger.getLogger(ClusteringAlgorithm.class);
    @Override
    public Cluster performClustering(WeightedGraph<Place, DirectedEdge> graph, LinkageStrategy linkageStrategy) {
        long startTime=System.nanoTime();
    /* Setup model */
        List<Cluster> clusters = createClusters(graph.vertexSet());
        DistanceMap linkages = createLinkages(graph, clusters);

    /* Process */
        HierarchyBuilder builder = new HierarchyBuilder(clusters, linkages);
        while (!builder.isTreeComplete()) {
            builder.agglomerate(linkageStrategy);
        }

        long endTime= System.nanoTime()-startTime;
        Statistics.getInstance().addClusterTime(this.getClass().getSimpleName(), com.dschulz.datagenerator.Statistics.Enum.Cluster.CLUSTERING_TIME,endTime);
        return builder.getRootCluster();
    }

    @Override
    public Cluster performWeightedClustering(WeightedGraph<Place, DirectedEdge> graph, LinkageStrategy linkageStrategy) {

        long startTime= System.nanoTime();
        logger.info("Performing Clustering");
    /* Setup model */
        List<Cluster> clusters = createWeightedClusters(graph.vertexSet());
        DistanceMap linkages = createLinkages(graph, clusters);

    /* Process */
        HierarchyBuilder builder = new HierarchyBuilder(clusters, linkages);
        while (!builder.isTreeComplete()) {
            builder.agglomerate(linkageStrategy);
        }
        long endTime= System.nanoTime()-startTime;
        logger.info("Performing Clustering took "+endTime+" ns");
        Statistics.getInstance().addClusterTime(this.getClass().getSimpleName(), com.dschulz.datagenerator.Statistics.Enum.Cluster.WEIGHTED_CLUSTERING_TIME,endTime);
        return builder.getRootCluster();
    }


    private DistanceMap createLinkages(WeightedGraph<Place,DirectedEdge> graph,
                                       List<Cluster> clusters) {
        long startTime= System.nanoTime();
        logger.info("Creating Cluster Linkages");
        DistanceMap linkages = new DistanceMap();
        for (int col = 0; col < clusters.size(); col++) {
            for (int row = col + 1; row < clusters.size(); row++) {
                ClusterPair link = new ClusterPair();
                Cluster lCluster = clusters.get(col);
                Cluster rCluster = clusters.get(row);
                try {
                    double tEdgeWeight = graph.getEdge(lCluster.getPlace(), rCluster.getPlace()).getWeight();
                    link.setLinkageDistance(tEdgeWeight);
                    link.setlCluster(lCluster);
                    link.setrCluster(rCluster);
                    linkages.add(link);
                }catch (Exception e){
                    logger.error("IS null",e);
                }
            }
        }
        long endTime= System.nanoTime()-startTime;
        logger.info("Creating Cluster linkages took "+endTime+" ns");
        Statistics.getInstance().addClusterTime(this.getClass().getSimpleName(), com.dschulz.datagenerator.Statistics.Enum.Cluster.CREATE_LINKAGES_TIME,endTime);
        return linkages;
    }

    private List<Cluster> createClusters(Set<Place> nodes) {
        long startTime= System.nanoTime();
        logger.info("Creating Cluster");
        List<Cluster> clusters = new ArrayList<Cluster>();
        for (Place place : nodes) {
            Cluster cluster = new Cluster(place);
            clusters.add(cluster);
        }
        long endTime= System.nanoTime()-startTime;
        logger.info("Creating Cluster took "+endTime+" ns");
        Statistics.getInstance().addClusterTime(this.getClass().getSimpleName(), com.dschulz.datagenerator.Statistics.Enum.Cluster.CREATE_CLUSTER_TIME,endTime);
        return clusters;
    }

    private List<Cluster> createWeightedClusters(Set<Place> nodes) {
        long startTime = System.nanoTime();
        logger.info("Creating weighted Cluster");
        List<Cluster> clusters = new ArrayList<Cluster>();
        for (Place place : nodes) {
            Cluster cluster = new Cluster(place);
            cluster.setDistance(new Distance(0.0, place.getInterestFactor()));
            clusters.add(cluster);
        }
        long endTime= System.nanoTime()-startTime;
        logger.info("Creating weighted Cluster took "+endTime+" ns");
        Statistics.getInstance().addClusterTime(this.getClass().getSimpleName(), com.dschulz.datagenerator.Statistics.Enum.Cluster.CREATE_WEIGHTED_CLUSTER_TIME,endTime);
        return clusters;
    }

    @Override
    public Cluster performClustering(double[][] distances, String[] clusterNames, LinkageStrategy linkageStrategy) {
        throw new NotImplementedException();
    }

    @Override
    public Cluster performWeightedClustering(double[][] distances, String[] clusterNames, double[] weights, LinkageStrategy linkageStrategy) {
        throw new NotImplementedException();
    }

}