package com.dschulz.datagenerator.Visitor.Provider;

import com.dschulz.datagenerator.Cluster.Cluster;
import com.dschulz.datagenerator.Const;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStation;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.graphhopper.util.shapes.GHPoint;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import org.apache.log4j.Logger;
import org.bson.Document;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.*;

/**
 * Created by daniel on 06.05.16.
 */
public class MongoDataProvider implements DataProvider {
    private static MongoClient instance;
    static final Logger logger = Logger.getLogger("DataProvider");
    private static MongoDatabase getInstance() {
        if (instance == null) {
            instance = new MongoClient(Const.MONGO_ADDRESS, Const.MONGO_PORT);
            DropCollections();
        }
        return instance.getDatabase(Const.MONGO_DB_NAME);
    }

    public static void DropCollections() {
        try {
            getInstance().getCollection(Const.MONGO_COLLECTION_VISITOR).drop();
            getInstance().getCollection("stations").drop();
            getInstance().getCollection("cluster").drop();
        } catch (Throwable t) {
            logger.error("DropCollections failed", t);
        }
    }


    @Override
    public void createTransmitterStations(List<TransmitterStation> stationList) throws NotImplementedException {
        Iterator<TransmitterStation> it=stationList.iterator();
        while(it.hasNext()){
            TransmitterStation station=it.next();
            getInstance().getCollection("stations").insertOne(
                    new Document()
                            .append("position",positionToDocument(station.getPosition()))
                            .append("radius",station.getRadius())
            );
        }
    }

    @Override
    public void createCluster(Cluster rootCluster) throws NotImplementedException {
        createClusterDocuments(rootCluster);
    }

    private Document createClusterDocuments(Cluster rootCluster){
        Document children= null;
        if(rootCluster.getChildren().size()!=0){
            children= new Document();
            int i=0;
            for(Cluster child: rootCluster.getChildren()) {
                children.append(i+"",createClusterDocuments(child));
            }
        }
        Document thisCluster= new Document()
                .append("name",rootCluster.getName())
                .append("position",positionToDocument(rootCluster.getPosition()))
                .append("weight",rootCluster.getWeightValue())
                .append("distance",rootCluster.getDistanceValue());
        if(children!=null){
            thisCluster.append("children",children);
        }
        getInstance().getCollection("cluster").insertOne(thisCluster);
        return thisCluster;
    }

    @Override
    public void createRecord(Visitor visitor) {
        if (Const.UPDATE_DURING_SIMULATION) {
            String log=visitor.getId()+":{lat"+visitor.getPosition().getLat()+";lon"+visitor.getPosition().getLon()+"}@"+visitor.getTimingId()+" Provider::Create";
            logger.info(log);
            getInstance().getCollection(Const.MONGO_COLLECTION_VISITOR).insertOne(
                    new Document()
                            .append("position", this.positionMapToDocument(visitor.getPositionMap()))
                            .append("_id", visitor.getId())
                            .append("interestReduce", visitor.getInterestReducePerTime())
                            .append("interestNeeded", visitor.getInterestNeeded())
                            .append("goalStrategy", visitor.getGoalStrategy().getName())
                            .append("enjoyingStrategy", visitor.getEnjoyingStrategy().getName())
                            .append("routingStrategy", visitor.getRoutingStrategy().getName())
            );
        }
    }

    @Override
    public void updateRecord(Visitor visitor) {
        if (Const.UPDATE_DURING_SIMULATION) {

            String log=visitor.getId()+":{lat"+visitor.getPosition().getLat()+";lon"+visitor.getPosition().getLon()+"}@"+visitor.getTimingId()+" Provider::Update";
            logger.info(log);
            Document findQuery = new Document("_id", visitor.getId());
            Document setQuery = new Document("position", this.positionMapToDocument(visitor.getPositionMap()));
            Document updateQuery = new Document("$set", setQuery);
            getInstance().getCollection(Const.MONGO_COLLECTION_VISITOR).updateOne(findQuery, updateQuery);
        }
    }

    @Override
    public void updateRecord(Visitor visitor, GHPoint correctedPosition) {
        if (Const.UPDATE_DURING_SIMULATION) {

            String log=visitor.getId()+":{lat"+visitor.getPosition().getLat()+";lon"+visitor.getPosition().getLon()+"}@"+visitor.getTimingId()+" Provider::Update";
            logger.info(log);
            Document findQuery = new Document("_id", visitor.getId());
            Document setQuery = new Document("position", this.positionMapToDocument(visitor.getPositionMap()));
            Document setQuery2 = new Document("correctedPosition", this.positionMapToDocument(visitor.getCorrectedPositionMap()));
            Document updateQuery = new Document("$set", setQuery);
            Document updateQuery2 =new Document("$set", setQuery2);
            getInstance().getCollection(Const.MONGO_COLLECTION_VISITOR).updateOne(findQuery, updateQuery);
            getInstance().getCollection(Const.MONGO_COLLECTION_VISITOR).updateOne(findQuery, updateQuery2);
        }
    }

    @Override
    public void finalizeRecord(Visitor visitor) {
        if (!Const.UPDATE_DURING_SIMULATION) {
            String log=visitor.getId()+":{lat"+visitor.getPosition().getLat()+";lon"+visitor.getPosition().getLon()+"}@"+visitor.getTimingId()+" Provider::Finalize";
            logger.info(log);
            getInstance().getCollection(Const.MONGO_COLLECTION_VISITOR).insertOne(
                    new Document()
                            .append("correctedPosition", this.positionMapToDocument(visitor.getCorrectedPositionMap()))
                            .append("position", this.positionMapToDocument(visitor.getPositionMap()))
                            .append("_id", visitor.getId())
                            .append("interestReduce", visitor.getInterestReducePerTime())
                            .append("interestNeeded", visitor.getInterestNeeded())
                            .append("goalStrategy", visitor.getGoalStrategy().getName())
                            .append("enjoyingStrategy", visitor.getEnjoyingStrategy().getName())
                            .append("routingStrategy", visitor.getRoutingStrategy().getName())
            );
        }
    }



    private HashMap<String,Document> positionMapToDocument(HashMap<Long,GHPoint> positionMap){
        HashMap<String,Document> mongoPositionMap=new HashMap<String,Document>();
        for(Map.Entry<Long, GHPoint> entry : positionMap.entrySet()) {
            Long key = entry.getKey();
            GHPoint value = entry.getValue();
            if(value==null){
                mongoPositionMap.put((key.toString()),null);
            }else{
            mongoPositionMap.put(key.toString(),new Document()
                    .append("lat",value.getLat())
                    .append("lon",value.getLon())
            );}
        }
        return mongoPositionMap;
    }

    private Document positionToDocument(GHPoint position){
        Document doc = new Document()
                .append("lat",position.getLat())
                .append("lon",position.getLon());
        return doc;
    }
}
