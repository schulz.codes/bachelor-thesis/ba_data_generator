package com.dschulz.datagenerator.Visitor.Provider;

import com.dschulz.datagenerator.Cluster.Cluster;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStation;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.graphhopper.util.shapes.GHPoint;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;

/**
 * Created by daniel on 28.04.16.
 */
public interface DataProvider {
    void createRecord(Visitor visitor);

    void updateRecord(Visitor visitor);

    void updateRecord(Visitor visitor, GHPoint correctedPosition) throws NotImplementedException;

    void finalizeRecord(Visitor visitor);

    void createTransmitterStations(List<TransmitterStation> stationList) throws NotImplementedException;

    void createCluster(Cluster rootCluster) throws NotImplementedException;
}
