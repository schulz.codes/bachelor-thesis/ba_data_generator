package com.dschulz.datagenerator.Visitor.BehaviourStrategies.RoutingStrategy;

import com.dschulz.datagenerator.Visitor.Visitor;
import com.graphhopper.GHResponse;
import com.graphhopper.PathWrapper;
import com.graphhopper.util.shapes.GHPoint;

/**
 * Created by daniel on 16.04.16.
 */
public interface VisitorRoutingStrategy {
    PathWrapper getRoute(Visitor visitor, GHPoint nextGoal);

    GHResponse route(GHPoint From, GHPoint to);

    String getName();
}
