package com.dschulz.datagenerator.Statistics;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

public class CSVUtils {

    private static final char DEFAULT_SEPARATOR = ',';

    public static void writeLine(Writer w, List<String> values) throws IOException {
        writeLine(w, values, DEFAULT_SEPARATOR, ' ');
    }

    public static void writeLine(Writer w, List<String> values, char separators) throws IOException {
        writeLine(w, values, separators, ' ');
    }

    //https://tools.ietf.org/html/rfc4180
    private static String followCVSformat(String value) {

        String result = value;
        if (result.contains("\"")) {
            result = result.replace("\"", "\"\"");
        }
        return result;

    }

    public static void writeLine(Writer w, List<String> values, char separators, char customQuote) throws IOException {

        boolean first = true;

        //default customQuote is empty

        if (separators == ' ') {
            separators = DEFAULT_SEPARATOR;
        }

        StringBuilder sb = new StringBuilder();
        for (String value : values) {
            if (!first) {
                sb.append(separators);
            }
            if (customQuote == ' ') {
                sb.append(followCVSformat(value));
            } else {
                sb.append(customQuote).append(followCVSformat(value)).append(customQuote);
            }

            first = false;
        }
        sb.append("\n");
        w.append(sb.toString());


    }

    public static void toCSV(String filename, TreeMap<String, ArrayList<Double>> data,ArrayList<String> params) throws IOException {
        FileWriter writer = new FileWriter(filename);

        ArrayList<String> keys=new ArrayList(data.keySet());
        ArrayList<String>keyShow=new ArrayList<String>();
        keyShow.add("ID");
        keyShow.addAll(keys);
        CSVUtils.writeLine(writer,keyShow);
        for(int i=0; i<params.size();i++) {
            ArrayList<String> values = new ArrayList<>();
            values.add(params.get(i));
            for (String key : keys) {
                if (data.get(key).size() <= i) {
                    values.add("");
                } else {
                    values.add(data.get(key).get(i).toString());
                }
            }
            CSVUtils.writeLine(writer, values);
        }
        writer.flush();
        writer.close();

    }

}