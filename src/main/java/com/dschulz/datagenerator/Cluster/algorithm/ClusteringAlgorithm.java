/*******************************************************************************
 * Copyright 2013 Lars Behnke
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.dschulz.datagenerator.Cluster.Algorithm;

import com.dschulz.datagenerator.Cluster.Cluster;
import com.dschulz.datagenerator.Cluster.Stategies.LinkageStrategy;
import com.dschulz.datagenerator.Cluster.DirectedEdge;
import com.dschulz.datagenerator.Place.Place;
import org.jgrapht.WeightedGraph;

public interface ClusteringAlgorithm {

    Cluster performClustering(WeightedGraph<Place, DirectedEdge> graph, LinkageStrategy linkageStrategy);

    Cluster performWeightedClustering(WeightedGraph<Place, DirectedEdge> graph, LinkageStrategy linkageStrategy);

    Cluster performClustering(double[][] distances, String[] clusterNames, LinkageStrategy linkageStrategy);

    Cluster performWeightedClustering(double[][] distances, String[] clusterNames, double[] weights, LinkageStrategy linkageStrategy);

}
