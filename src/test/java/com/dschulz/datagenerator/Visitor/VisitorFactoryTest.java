package com.dschulz.datagenerator.Visitor;

import com.dschulz.datagenerator.App;
import com.dschulz.datagenerator.Math.Functions;
import com.dschulz.datagenerator.Place.PlaceFactory;
import com.dschulz.datagenerator.Routing.Hopper;
import com.dschulz.datagenerator.SampleData;
import com.dschulz.datagenerator.TestClass;
import com.dschulz.datagenerator.Visitor.BehaviourStrategies.GoalStrategy.VisitorGoalStrategy;
import com.dschulz.datagenerator.Visitor.BehaviourStrategies.RoutingStrategy.VisitorRoutingStrategy;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStationFactory;
import com.graphhopper.util.shapes.GHPoint;
import org.apache.log4j.BasicConfigurator;
import org.junit.Before;
import org.junit.Test;
import org.reflections.Reflections;

import java.lang.reflect.Field;
import java.util.Iterator;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by daniel on 08.08.16.
 */
public class VisitorFactoryTest extends TestClass {
    public static GHPoint centerPoint = new GHPoint(50.682903, 10.937857);

    @Before
    public void setUp() throws Exception{
        super.setUp();
    }
    @Test
    public void getVisitorsCount() throws Exception {
        assertTrue(VisitorFactory.getInstance().getVisitorsCount()==0);
        assertTrue(VisitorFactory.getInstance().getVisitorList().size()==VisitorFactory.getInstance().getVisitorsCount());
        List<Visitor> list=VisitorFactory.getInstance().InitVisitors(centerPoint,2,5);
        assertTrue(VisitorFactory.getInstance().getVisitorsCount()==5);
    }

    @Test
    public void getActiveVisitorsCount() throws Exception {
        assertTrue(VisitorFactory.getInstance().getVisitorsCount()==0);
        assertTrue(VisitorFactory.getInstance().getVisitorList().size()==VisitorFactory.getInstance().getVisitorsCount());
        List<Visitor> list=VisitorFactory.getInstance().InitVisitors(centerPoint,2,5);
        assertTrue(VisitorFactory.getInstance().getVisitorsCount()==5);
        int visitorCount=5;
        while(VisitorFactory.getInstance().getActiveVisitorsCount()>0){
            Iterator<Visitor> it=list.iterator();
            while(it.hasNext()){
                Visitor visitor=it.next();
                visitor.run();
                if(visitor.isFinished()){
                    visitorCount--;
                    it.remove();
                    assertTrue(VisitorFactory.getInstance().getActiveVisitorsCount()==visitorCount);
                }
            }

        }

    }

    @Test
    public void testGetBehaviourReflections() throws Exception {
        Reflections reflections = VisitorFactory.getInstance().getBehaviourReflections();
        assertNotNull(reflections);
        assertTrue(reflections.getSubTypesOf(VisitorGoalStrategy.class).size() > 0);
        assertTrue(reflections.getSubTypesOf(VisitorRoutingStrategy.class).size() > 0);
        assertTrue(reflections.getSubTypesOf(VisitorGoalStrategy.class).size() > 0);
    }



    @Test
    public void initVisitors() throws Exception {
        VisitorFactory.getInstance().InitVisitors(centerPoint, 2, 5);
        assertEquals(5, VisitorFactory.getInstance().getVisitorsCount());
        for (Visitor tVisitor : VisitorFactory.getInstance().getVisitorList()) {
            assertTrue(Functions.Distance(centerPoint, tVisitor.getPosition()) < 2);
        }
    }

}