package com.dschulz.datagenerator.Visitor.Transmitter.PositionReconstructionStrategies;

import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStation;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.graphhopper.util.shapes.GHPoint;

import java.util.TreeMap;

/**
 * Created by daniel on 15.06.16.
 */
public interface PositionReconstructionStrategy {


    GHPoint getReconstructedPosition(Visitor visitor, TreeMap<Double, TransmitterStation> stationDistanceMap);
}
