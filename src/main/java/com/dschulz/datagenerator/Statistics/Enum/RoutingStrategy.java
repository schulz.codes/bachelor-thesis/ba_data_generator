package com.dschulz.datagenerator.Statistics.Enum;

/**
 * Created by daniel on 21.07.16.
 */
public enum RoutingStrategy {
    FROM_CACHE_TIME, CALCULATED_TIME
}
