package com.dschulz.datagenerator.Visitor.BehaviourStrategies.GoalStrategy;

import com.dschulz.datagenerator.Place.PlaceFactory;
import com.dschulz.datagenerator.Statistics.Enum.GoalStrategy;
import com.dschulz.datagenerator.Statistics.Statistics;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.graphhopper.GHResponse;
import com.graphhopper.PathWrapper;
import com.graphhopper.util.shapes.GHPoint;
import com.dschulz.datagenerator.Cluster.Cluster;
import org.apache.log4j.Logger;

/**
 * Created by daniel on 16.04.16.
 */
public class mostInterestingClusterFirstGoalStrategy implements VisitorGoalStrategy{

    static final Logger logger = Logger.getLogger(VisitorGoalStrategy.class);
    @Override
    public GHPoint findGoal(Visitor visitor) {
        long startTime=System.nanoTime();
        Cluster tCluster = PlaceFactory.getInstance().getClustered();
        Cluster tChildClusterSave = null;
        PathWrapper tBestPath = null;
        if (visitor.getInterestNeeded() <= visitor.getInterestReducePerTime()) {
            long endTime=System.nanoTime()-startTime;
            Statistics.getInstance().addGoalStrategyTime(this.getClass().getSimpleName(), GoalStrategy.NO_INTEREST_TIME,endTime);
            return visitor.getHome();
        } else {
            while (tCluster.getChildren().size() != 0) {
                for (Cluster childCluster : tCluster.getChildren()) {;
                    GHResponse tResp = visitor.getRoutingStrategy().route(visitor.getPosition(),childCluster.getPosition());
                    try {
                        PathWrapper tRespPath = tResp.getBest();//could be empty?!
                        if (!visitor.visitedAllInCluster(childCluster) && !visitor.visited(childCluster)) {
                            if (tBestPath == null) {
                                tBestPath = tRespPath;
                                tChildClusterSave = childCluster;
                            } else {
                                if (tChildClusterSave.getWeightValue() < childCluster.getWeightValue()) {
                                    tBestPath = tRespPath;
                                    tChildClusterSave = childCluster;
                                }
                            }
                        }
                    } catch (Throwable t) {
                        logger.error("Graphooppper Error",t);
                    }
                }
                if (tBestPath == null) {
                    long endTime=System.nanoTime()-startTime;
                    Statistics.getInstance().addGoalStrategyTime(this.getClass().getSimpleName(),GoalStrategy.ALL_SEEN_TIME,endTime);
                    return visitor.getHome();// hat alles gesehen
                }
                tBestPath = null;
                tCluster = tChildClusterSave;
            }
            long endTime=System.nanoTime()-startTime;
            Statistics.getInstance().addGoalStrategyTime(this.getClass().getSimpleName(),GoalStrategy.FOUND_GOAL_TIME,endTime);
            return tCluster.getPlace().getPosition();
        }
    }
    @Override
    public String getName() {
        return "MICF";
    }


}
