package com.dschulz.datagenerator.Visitor;

import com.dschulz.datagenerator.Place.PlaceFactory;
import com.dschulz.datagenerator.Visitor.Provider.DataProvider;
import com.dschulz.datagenerator.Routing.Hopper;
import com.dschulz.datagenerator.Statistics.Statistics;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStation;
import com.graphhopper.PathWrapper;
import com.graphhopper.routing.util.EdgeFilter;
import com.graphhopper.storage.index.LocationIndex;
import com.graphhopper.storage.index.QueryResult;
import com.graphhopper.util.Instruction;
import com.graphhopper.util.shapes.GHPoint;
import com.dschulz.datagenerator.Cluster.Cluster;
import com.dschulz.datagenerator.App;
import com.dschulz.datagenerator.Const;
import com.dschulz.datagenerator.Math.Functions;
import com.dschulz.datagenerator.Math.MathDistributions;
import com.dschulz.datagenerator.Place.Place;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterLogger;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStationFactory;
import com.dschulz.datagenerator.Visitor.BehaviourStrategies.EnjoyingStrategy.VisitorEnjoyStrategy;
import com.dschulz.datagenerator.Visitor.BehaviourStrategies.GoalStrategy.VisitorGoalStrategy;
import com.dschulz.datagenerator.Visitor.BehaviourStrategies.RoutingStrategy.VisitorRoutingStrategy;
import org.apache.log4j.Logger;
import org.reflections.Reflections;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;


import java.util.*;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

/**
 * Created by daniel on 08.04.16.
 */
public class Visitor implements Runnable {

    //region Variables
    //region static Variables
    private static final Logger logger = Logger.getLogger("Visitor");
    //endregion

    //region Visitor Properties
    private long Id;
    private GHPoint Home;
    private GHPoint Position;
    private double InterestNeed;
    private double InterestReducePerTime;
    private double MeanDistancePerStep;
    private VisitorGoalStrategy goalStrategy;
    private VisitorRoutingStrategy routingStrategy;

    private VisitorEnjoyStrategy enjoyingStrategy;
    private DataProvider dataProvider;
    private GHPoint lastCorrectePosition;
    private double distanceWalked=0;
    //endregion

    //region Movement History
    private HashMap<Long, GHPoint> positionMap = new HashMap<>();
    private HashMap<Long, GHPoint> correctedPositionMap = new HashMap<>();
    private ArrayList<Place> visitedPlaces = new ArrayList<>();
    //endregion

    //region Runnable Variables
    private boolean enjoying = false;

    private ScheduledFuture<?> future=null;

    private boolean finished=false;
    //endregion

    //region Timing Variables
    private long startTimeOffset;
    private int visitorStep = 0;
    private int walkingSteps=0;
    private int enjoyingSteps=0;

    //endregion

    //region Walking() Variables
    private GHPoint nextGoal = null;
    private PathWrapper path = null;


    private int InstructionIndex = 0;
    private int PointIndex = 0;
    private double PlaceAttachement = -1;
    //endregion

    //region Reflections

    //endregion
    //endregion

    //region Runnable::Interface
    public void run() {
        try {
            if(Const.BATCH_PROCESSING) {
                while (!finished) {
                    long startRun = System.nanoTime();
                    DoSimulation();
                    long runDur = System.nanoTime() - startRun;
                }
            }else {
                if(!finished) {
                    long startRun = System.nanoTime();
                    DoSimulation();
                    long runDur = System.nanoTime() - startRun;
                }
            }
            if(finished){
                this.visitedPlaces=null;
                this.positionMap=null;
                this.correctedPositionMap=null;
                this.goalStrategy=null;
                this.routingStrategy=null;
                this.enjoyingStrategy=null;

            }

        } catch (Throwable t) {  // Catch Throwable rather than Exception (a subclass).
            logger.error("Caught exception in Runnable. StackTrace:", t);
        }
    }
    //endregion

    //region Init

    public Visitor(long id, GHPoint center, double distance) {
        this.startTimeOffset=System.currentTimeMillis()- App.getSimulationStart();

        if(Const.BATCH_PROCESSING && VisitorFactory.getInstance().getVisitorsCount() >=Const.MAX_VISITORS_SAME_TIME){
            try {
                this.startTimeOffset += VisitorFactory.getInstance().getTimeFinished().poll();
            }catch (Exception e){
                logger.error("error",e);
            }
        }
        this.Id = id;
        InterestNeed = MathDistributions.RandomInRange(Const.MIN_INTEREST_NEED, Const.MAX_INTEREST_NEED);
        InterestReducePerTime = MathDistributions.RandomInRange(Const.MIN_INTEREST_REDUCE_PER_TIME, Const.MAX_INTEREST_REDUCE_PER_TIME);
        MeanDistancePerStep = MathDistributions.NormalRandom(Const.VISITOR_DISTANCE_PER_SIMULATION_STEP, Const.VISITOR_VARIANCE_PER_SIMULATION_STEP);
        this.Home=this.CalculateHome(center,distance);
        this.Position = this.Home;

        //region Reflection
        Reflections reflections=VisitorFactory.getInstance().getBehaviourReflections();
        Set<Class<? extends VisitorGoalStrategy>> visitorGoalStrategies = reflections.getSubTypesOf(VisitorGoalStrategy.class);
        Set<Class<? extends VisitorRoutingStrategy>> visitorRoutingStrategies = reflections.getSubTypesOf(VisitorRoutingStrategy.class);
        Set<Class<? extends VisitorEnjoyStrategy>> visitorEnjoyingStrategies = reflections.getSubTypesOf(VisitorEnjoyStrategy.class);
        int goalKey = new Random().nextInt(visitorGoalStrategies.size());
        int movementKey = new Random().nextInt(visitorRoutingStrategies.size());
        int enjoyingKey = new Random().nextInt(visitorEnjoyingStrategies.size());
        int i = 0;

        for (Class<? extends VisitorGoalStrategy> strategy : visitorGoalStrategies) {
            if (i == goalKey) {
                try {
                    this.goalStrategy = strategy.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
            i++;
        }
        int j = 0;
        for (Class<? extends VisitorRoutingStrategy> strategy : visitorRoutingStrategies) {
            if (j == movementKey) {
                try {
                    this.routingStrategy = strategy.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
            j++;
        }
        j = 0;
        for (Class<? extends VisitorEnjoyStrategy> strategy : visitorEnjoyingStrategies) {
            if (j == enjoyingKey) {
                try {
                    this.enjoyingStrategy = strategy.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
            j++;
        }
        //endregion
        try {
            this.dataProvider = Const.VISITOR_DATA_PROVIDER.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();

        }
        this.dataProvider.createRecord(this);

    }

    public Visitor(long id, GHPoint home) {
        this.startTimeOffset=System.currentTimeMillis()- App.getSimulationStart();

        if(Const.BATCH_PROCESSING && VisitorFactory.getInstance().getVisitorsCount() >=Const.MAX_VISITORS_SAME_TIME){
            try {
                this.startTimeOffset += VisitorFactory.getInstance().getTimeFinished().poll();
            }catch (Exception e){
                logger.error("error",e);
            }
        }
        this.Id = id;
        InterestNeed = MathDistributions.RandomInRange(Const.MIN_INTEREST_NEED, Const.MAX_INTEREST_NEED);
        InterestReducePerTime = MathDistributions.RandomInRange(Const.MIN_INTEREST_REDUCE_PER_TIME, Const.MAX_INTEREST_REDUCE_PER_TIME);
        MeanDistancePerStep = MathDistributions.NormalRandom(Const.VISITOR_DISTANCE_PER_SIMULATION_STEP, Const.VISITOR_VARIANCE_PER_SIMULATION_STEP);
        this.Home=home;
        this.Position = home;

        //region Reflection
        Reflections reflections=VisitorFactory.getInstance().getBehaviourReflections();
        Set<Class<? extends VisitorGoalStrategy>> visitorGoalStrategies = reflections.getSubTypesOf(VisitorGoalStrategy.class);
        Set<Class<? extends VisitorRoutingStrategy>> visitorRoutingStrategies = reflections.getSubTypesOf(VisitorRoutingStrategy.class);
        Set<Class<? extends VisitorEnjoyStrategy>> visitorEnjoyingStrategies = reflections.getSubTypesOf(VisitorEnjoyStrategy.class);
        int goalKey = new Random().nextInt(visitorGoalStrategies.size());
        int movementKey = new Random().nextInt(visitorRoutingStrategies.size());
        int enjoyingKey = new Random().nextInt(visitorEnjoyingStrategies.size());
        int i = 0;

        for (Class<? extends VisitorGoalStrategy> strategy : visitorGoalStrategies) {
            if (i == goalKey) {
                try {
                    this.goalStrategy = strategy.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
            i++;
        }
        int j = 0;
        for (Class<? extends VisitorRoutingStrategy> strategy : visitorRoutingStrategies) {
            if (j == movementKey) {
                try {
                    this.routingStrategy = strategy.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
            j++;
        }
        j = 0;
        for (Class<? extends VisitorEnjoyStrategy> strategy : visitorEnjoyingStrategies) {
            if (j == enjoyingKey) {
                try {
                    this.enjoyingStrategy = strategy.newInstance();
                } catch (InstantiationException e) {
                    e.printStackTrace();
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
                break;
            }
            j++;
        }
        //endregion
        try {
            this.dataProvider = Const.VISITOR_DATA_PROVIDER.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();

        }
        this.dataProvider.createRecord(this);
    }



    /**
     * Sets the Home Point for a Vistor in a Circle with radius distance around center
     *
     * @param center   From Which Center Point?
     * @param distance in KM
     */
    private GHPoint CalculateHome(GHPoint center, double distance) {
        GHPoint tHome= MathDistributions.EqualInCircle(center,distance);
        LocationIndex index = Hopper.getInstance().getLocationIndex();
        QueryResult qr = index.findClosest(tHome.getLat(), tHome.getLon(), EdgeFilter.ALL_EDGES);
        int node = qr.getClosestNode();
        Double tLat = Hopper.getInstance().getGraphHopperStorage().getNodeAccess().getLatitude(node);
        Double tLong = Hopper.getInstance().getGraphHopperStorage().getNodeAccess().getLongitude(node);
        return new GHPoint(tLat, tLong);
    }


    //endregion

    //region Simulation

    public void DoSimulation() {
        try {
            if (enjoying) {
                enjoyingSteps++;
                Enjoying();
            } else {
                walkingSteps++;
                Walking();
            }
            long timingID = this.getTimingId();
            this.getPositionMap().put(timingID,(GHPoint) this.getPosition());
            if(Const.TRANSMITTER_STATIONS) {
                if(visitorStep%Const.TRANSMITTER_STEPS_BETWEEN==0){
                    TreeMap<Double, TransmitterStation> recievedPositions =
                            TransmitterStationFactory.getInstance().getTransmittersInMaxRadius(this);
                    lastCorrectePosition= TransmitterLogger.getInstance().getReconstructedPosition(this,recievedPositions);
                }
               getCorrectedPositionMap().put(timingID, lastCorrectePosition);
                try {
                    if(lastCorrectePosition!=null) {
                        this.dataProvider.updateRecord(this, lastCorrectePosition);
                    }
                }catch (NotImplementedException e){
                    this.dataProvider.updateRecord(this);
                }
            }else{
                this.dataProvider.updateRecord(this);
            }
            this.visitorStep++;
        }catch(Exception e){
            logger.error("Simulation Ex",e);
        }
    }

    /**
     *
     */
    private void Enjoying() {
        if (this.PlaceAttachement == -1) {
            this.PlaceAttachement = MathDistributions.NormalRandom(Const.MEAN_PLACE_ATTACHEMENT, Const.VARIANCE_PLACE_ATTACHEMENT);
        }
        double reduceInterest = this.enjoyingStrategy.getInterestReduce(this, this.nextGoal);
        this.reduceInterestNeeded(reduceInterest);
        //logger.info("Enjoys rpI:"+this.PlaceAttachement);
        if (this.enjoyingStrategy.leave(this, this.nextGoal)) {
            //logger.info("Now Leaving");
            nextGoal = null;
            enjoying = false;
            PlaceAttachement = -1;
        }
    }

    /**
     * Simulates the WalkingProcess of a Vistor During the whole Process
     * <p>
     * First finds a Goal with goal Strategy and with RoutingStrategy Path to the Goal. This reoccurs after a user enjoyed
     * If a User has no Interest anymore he heads back Home otherwise he targets to a new InterestPoint
     */
    private void Walking() {
        //logger.info("Visitor "+Id+" Walks");
        if (this.nextGoal == null) {
            this.nextGoal = this.goalStrategy.findGoal(this);
        }
        if (this.path == null) {
            this.path = this.routingStrategy.getRoute(this, this.nextGoal);
            if (this.getPositionMap().size() == 0) {//resets Home to Point On Street before Move
                this.Home = new GHPoint(this.path.getPoints().getLat(0), this.path.getPoints().getLon(0));
            }
            this.distanceWalked+=this.getPath().getDistance();
        }
        //From here the Instructions are walked through until next Goal is reached.
        Instruction tNextGoal = this.path.getInstructions().get(InstructionIndex);
        this.Position = this.getNextPosition(tNextGoal,PointIndex);
        //logger.info("NextPos ="+this.Position.toString());
        if (this.Position.equals(tNextGoal.getPoints().toGHPoint(PointIndex))) {
            PointIndex+=1;
            if(PointIndex==tNextGoal.getPoints().getSize()){
                //logger.info(this.getTimingId()+": "+this.getId() + " fulfilled Instruction, now next Instruction");
                InstructionIndex++;
                PointIndex = 0;
                if (InstructionIndex == this.path.getInstructions().getSize()) {
                    if (nextGoal.equals(this.getHome())) {
                        VisitorFactory.getInstance().getTimeFinished().add(this.getTimingId());
                        this.dataProvider.finalizeRecord(this);
                        //logger.info(this.getTimingId()+": "+this.getId() + ": home");
                        Statistics.getInstance().addVisitorStatistics(this);
                        Statistics.getInstance().addVisitedPlaces(this);
                        VisitorFactory.getInstance().ReturnHome(this);
                        System.out.print(".");
                        //Set null to enable GC these fields
                        finished = true;
                        if (this.getFuture() != null) {
                            this.getFuture().cancel(false);
                        }
                    } else {
                        //logger.info(this.getTimingId()+":"+ this.getId() + ":"+ PlaceFactory.getInstance().getPlaceToPosition(nextGoal).getName());
                        getVisitedPlaces().add(PlaceFactory.getInstance().getPlaceToPosition(nextGoal));
                        InstructionIndex = 0;
                        path = null;
                        enjoying = true;//sets enjoying to true so next round visitor goes into enjoyingMode
                    }
                }
            }
        }
    }

    //endregion

    //region Visited Check
    /**
     * checks if a visitor visited all Places in a Cluster
     *
     * @param cluster The Cluster which should be checked
     * @return true if all places in cluster were visited
     */
    public boolean visitedAllInCluster(Cluster cluster) {
        if (cluster.countLeafs() > getVisitedPlaces().size()) {
            return false;
        }
        ArrayList<Place> tPlacesInCluster = cluster.getPlacesInCluster();
        return getVisitedPlaces().containsAll(tPlacesInCluster);
    }

    /**
     * Checks if a visitor visited a Place-Cluster(Cluster with Place!=null)
     *
     * @param cluster The Cluster which should be checked
     * @return true if visitor visited Place-Cluster else false
     */
    public boolean visited(Cluster cluster) {
        if (cluster.getPlace() == null) {
            return false;
        }
        for (Place tPlace : getVisitedPlaces()) {
            if (tPlace.getId() == cluster.getPlace().getId()) {
                return true;
            }
        }
        return false;
    }
    //endregion

    //region Private/Protected Functions

    /**
     * Calculates the Next Point Position of a visitor which is heading to his next goal
     * Uses Bearing between current Position and nextGoal and moves Normal distributed Value MeanDistancePerStep to
     * his nextGoal
     *
     * @param goal which goalPoint should be reached next?
     * @return GHPoint of next step in the goalDirection
     */
    private GHPoint getNextPosition(Instruction goal, int PointIndex) {
        double dist = MathDistributions.NormalRandom(this.MeanDistancePerStep, Const.VISITOR_MEAN_VARIANCE_PER_SIMULATION_STEP);//KM in Direction

        GHPoint goalPoint = goal.getPoints().toGHPoint(PointIndex);
        double brng = Functions.DegreeBearing(this.getPosition(), goalPoint);
        GHPoint newPosition = Functions.MoveDistanceInDirection(this.Position, brng, dist);

        if ((newPosition == null) || (goalPoint == null)) {
            logger.info("one Point null");
        } else {
            double DistToPoint = Functions.Distance(newPosition, goalPoint);
            //logger.info("Distance to Goal " + DistToPoint);
            if (DistToPoint < dist) {
                return goalPoint;
            }
        }
        return newPosition;
    }



    //endregion

    //region Getter/Setter



    public HashMap<Long, GHPoint> getPositionMap(){return this.positionMap;}
    public HashMap<Long, GHPoint> getCorrectedPositionMap(){return this.correctedPositionMap;}

    public Long getTimingId(){return this.startTimeOffset+this.visitorStep*Const.VISITOR_SLEEPTIME;}

    public GHPoint getHome() {
        return this.Home;
    }

    public GHPoint getPosition() {
        return this.Position;
    }

    public double getInterestNeeded() {
        return this.InterestNeed;
    }

    public double getPlaceAttachement() {
        return this.PlaceAttachement;
    }

    public double getInterestReducePerTime() {
        return this.InterestReducePerTime;
    }

    public long getId() {
        return this.Id;
    }

    public ArrayList<Place> getVisitedPlaces() {
        return visitedPlaces;
    }
    public VisitorEnjoyStrategy getEnjoyingStrategy() {
        return this.enjoyingStrategy;
    }

    public VisitorRoutingStrategy getRoutingStrategy() {
        return this.routingStrategy;
    }

    public VisitorGoalStrategy getGoalStrategy() {
        return this.goalStrategy;
    }

    public int getInstructionIndex() {
        return InstructionIndex;
    }

    public int getPointIndex() {
        return PointIndex;
    }

    public PathWrapper getPath() {
        return path;
    }

    private void reduceInterestNeeded(double tInterest) {
        if (tInterest > this.InterestNeed) {
            this.InterestNeed = 0;
        } else {
            this.InterestNeed -= tInterest;
        }
    }
    private ScheduledFuture<?> getFuture() {
        return future;
    }

    public void setFuture(ScheduledFuture<?> future) {
        this.future=future;
    }

    public GHPoint getNextGoal() {
        return nextGoal;
    }

    public boolean isEnjoying() {
        return enjoying;
    }

    public GHPoint getLastCorrectePosition() {
        return lastCorrectePosition;
    }

    public DataProvider getDataProvider() {
        return dataProvider;
    }

    public double getInterestNeed() {
        return InterestNeed;
    }

    public long getStartTimeOffset() {
        return startTimeOffset;
    }

    public double getMeanDistancePerStep() {
        return MeanDistancePerStep;
    }

    public int getVisitorStep() {
        return visitorStep;
    }

    public double getDistanceWalked(){return distanceWalked;}

    public int getWalkingSteps(){return walkingSteps;}
    public int getEnjoyingSteps(){return enjoyingSteps;}

    public boolean isFinished() {
        return finished;
    }

    public void setEnjoyingStrategy(VisitorEnjoyStrategy enjoyingStrategy) {
        this.enjoyingStrategy = enjoyingStrategy;
    }

    public void setRoutingStrategy(VisitorRoutingStrategy routingStrategy) {
        this.routingStrategy = routingStrategy;
    }

    public void setGoalStrategy(VisitorGoalStrategy goalStrategy) {
        this.goalStrategy = goalStrategy;
    }
    //endregion
}
