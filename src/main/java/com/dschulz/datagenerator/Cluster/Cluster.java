/*******************************************************************************
 * Copyright 2013 Lars Behnke
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.dschulz.datagenerator.Cluster;

import com.dschulz.datagenerator.Place.Place;
import com.dschulz.datagenerator.Place.PlaceFactory;
import com.dschulz.datagenerator.Visitor.VisitorFactory;
import com.graphhopper.util.shapes.GHPoint;
import com.dschulz.datagenerator.App;
import com.dschulz.datagenerator.Const;
import com.dschulz.datagenerator.Visitor.Visitor;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Cluster implements Runnable {
    static final Logger logger = Logger.getLogger("Cluster");
    public static Semaphore accessSemaphore= new Semaphore(1);
    private String name;
    private Place place = null;
    private GHPoint position = null;
    private Cluster parent;
    private List<Cluster> children;
    private List<Cluster> allChildren;

    private Distance distance = new Distance();

    private double CurrentInterest = 0;

    public Distance getDistance() {
        return distance;
    }

    public Double getWeightValue() {
        return distance.getWeight();
    }

    public Double getDistanceValue() {
        return distance.getDistance();
    }

    public GHPoint getPosition() {
        if (this.place != null) {
            return this.place.getPosition();
        } else {
            return this.position;
        }
    }

    public void setDistance(Distance distance) {
        this.distance = distance;
    }

    public List<Cluster> getChildren() {
        if (children == null) {
            children = new ArrayList<Cluster>();
        }

        return children;
    }

    public void setChildren(List<Cluster> children) {
        this.children = children;
    }

    public Cluster getParent() {
        return parent;
    }

    public void setParent(Cluster parent) {
        this.parent = parent;
    }

    public Cluster(String name) {
        this.name = name;
        this.place = null;
    }

    public Cluster(Place place) {
        this.place = place;
        this.position = place.getPosition();
        this.name = place.getName();
    }

    public List<Cluster> getAllChildren() {
        if(this.allChildren==null) {
            this.allChildren = new ArrayList<Cluster>();
            if (this.getChildren() != null) {
                for (Cluster tChild : this.getChildren()) {
                    this.allChildren.addAll(tChild.getAllChildren());
                }
            }

            this.allChildren.add(this);
        }
        return this.allChildren;
    }

    public String getName() {
        return name;
    }

    public Place getPlace() {
        if (place != null) {
            return this.place;
        } else {
            return null;
        }
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addChild(Cluster cluster) {
        getChildren().add(cluster);
        GHPoint tPosition = cluster.getPosition();
        if (this.position == null) {
            this.position = tPosition;
        } else {
            // neue Cluster Position ist im mittelpunkt der Luftlinie
            GHPoint tCurrentPost = this.position;
            double tMidLong = ((tCurrentPost.getLon() + tPosition.getLon()) / 2);
            double tMidLat = ((tCurrentPost.getLat() + tPosition.getLat()) / 2);

            this.position = new GHPoint(tMidLat, tMidLong);
        }
    }

    public boolean contains(Cluster cluster) {
        return getChildren().contains(cluster);
    }

    public boolean contains(Place place) {
        ArrayList<Place> placesInCluster = this.getPlacesInCluster(this);
        for (Place tPlace : placesInCluster) {
            if (place.equals(tPlace)) {
                return true;
            }
        }
        return false;
    }

    public ArrayList<Place> getPlacesInCluster() {
        ArrayList<Place> list = new ArrayList<Place>();
        if (this.getPlace() != null) {
            list.add(this.getPlace());
            return list;
        } else {
            for (Cluster childCluster : this.getChildren()) {
                ArrayList<Place> tChildList = childCluster.getPlacesInCluster(childCluster);
                list.addAll(tChildList);
            }
            return list;
        }
    }

    public ArrayList<Place> getPlacesInCluster(Cluster cluster) {
        ArrayList<Place> list = new ArrayList<Place>();
        if (cluster.getPlace() != null) {
            list.add(cluster.getPlace());
            return list;
        } else {
            for (Cluster childCluster : cluster.getChildren()) {
                ArrayList<Place> tChildList = childCluster.getPlacesInCluster(childCluster);
                list.addAll(tChildList);
            }
            return list;
        }
    }

    @Override
    public String toString() {
        return "Cluster " + name;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Cluster other = (Cluster) obj;
        if (name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!name.equals(other.name)) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return (name == null) ? 0 : name.hashCode();
    }

    public boolean isLeaf() {
        return getChildren().size() == 0;
    }

    public int countLeafs() {
        return countLeafs(this, 0);
    }

    public int countLeafs(Cluster node, int count) {
        if (node.isLeaf()) count++;
        for (Cluster child : node.getChildren()) {
            count += child.countLeafs();
        }
        return count;
    }

    public void toConsole(int indent) {
        for (int i = 0; i < indent; i++) {
            System.out.print("  ");

        }
        String name = getName() + (isLeaf() ? " (leaf)" : "") + (distance != null ? "  distance: " + distance : "");
        System.out.println(name);
        for (Cluster child : getChildren()) {
            child.toConsole(indent + 1);
        }
    }

    public double getTotalDistance() {
        Double dist = getDistance() == null ? 0 : getDistance().getDistance();
        if (getChildren().size() > 0) {
            dist += children.get(0).getTotalDistance();
        }
        return dist;

    }

    @Override
    public void run() {
        try {
            if (VisitorFactory.getInstance().getVisitorsCount() < Const.MAX_VISITORS_TOTAL) {
                    double weight = this.getWeightValue();
                    double distance = this.getDistanceValue();
                    double weightedInterest;
                    if (distance == 0) {//Case for Places
                        distance = Const.SIMULATION_CLUSTER_PLACES_DISTANCE;
                    }
                    weightedInterest = (weight / (Math.PI*Math.pow((distance / 2),2)))*10000;
                    CurrentInterest += weightedInterest;
                    if (CurrentInterest > Const.SIMULATION_CLUSTER_INTEREST_THRESHOLD) {
                        int visitorCount = (int) Math.floor(CurrentInterest / Const.SIMULATION_CLUSTER_INTEREST_THRESHOLD);
                        if (visitorCount > Const.MAX_VISITORS_SAME_TIME){
                            visitorCount = Const.MAX_VISITORS_SAME_TIME;
                        }
                        try {
                            List<Visitor> createdVisitors = VisitorFactory.getInstance().InitVisitors(this.getPosition(), distance / 1000, visitorCount);
                            String tString = "";
                            for (int i = 0; i < createdVisitors.size(); i++) {
                                //logger.info(createdVisitors.get(i).getId() + ":{lat:" + createdVisitors.get(i).getPosition().getLat() + ";lon:" + createdVisitors.get(i).getPosition().getLon() + "}@" + createdVisitors.get(i).getTimingId() + " created by " + this.getName());
                            }
                            for (int i = 0; i < createdVisitors.size(); i++) {
                                if (i + 1 == createdVisitors.size()) {
                                    tString = tString + createdVisitors.get(i).getId();
                                } else {
                                    tString = tString + createdVisitors.get(i).getId() + ", ";
                                }
                            }

                            CurrentInterest = CurrentInterest % Const.SIMULATION_CLUSTER_INTEREST_THRESHOLD;
                        }catch(Exception e){
                            logger.error("Cluster Creation Error",e);
                        }
                    }
            }else{
                //logger.info("Shutdown Cluster ScheduledExecutor");
                PlaceFactory.getInstance().getPlaceExecutor().shutdown();
            }
        } catch (Throwable t) {  // Catch Throwable rather than Exception (a subclass).
            logger.error("Caught exception in ScheduledExecutorService.", t);
        }
    }
}
