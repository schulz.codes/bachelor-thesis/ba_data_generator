package com.dschulz.datagenerator.Visitor.Provider;

import com.dschulz.datagenerator.Cluster.Cluster;
import com.dschulz.datagenerator.Const;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStation;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.graphhopper.util.shapes.GHPoint;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.List;
import java.util.Map;

/**
 * Created by daniel on 07.05.16.
 */
public class HttpJsonDataProvider implements DataProvider {
    static final Logger logger = Logger.getLogger("DataProvider");
    @Override
    public void createRecord(Visitor visitor) {
        if(!Const.BATCH_PROCESSING) {
            String log=visitor.getId()+":{lat"+visitor.getPosition().getLat()+";lon"+visitor.getPosition().getLon()+"}@"+visitor.getTimingId()+" Provider::Create";
            logger.info(log);
            JSONObject visitorStrategies = new JSONObject();
            visitorStrategies.put("goal", visitor.getGoalStrategy().getName());
            visitorStrategies.put("routing", visitor.getRoutingStrategy().getName());
            visitorStrategies.put("enjoying", visitor.getEnjoyingStrategy().getName());

            JSONObject visitorJson = new JSONObject();
            visitorJson.put("id", visitor.getId());
            visitorJson.put("strategies", visitorStrategies);
            visitorJson.put("home", GHPointToJSON(visitor.getHome()));
            visitorJson.put("interest_needed", visitor.getInterestNeeded());
            visitorJson.put("interest_reduce_per_time", visitor.getInterestReducePerTime());
            visitorJson.put("position", GHPointToJSON(visitor.getPosition()));
            visitorJson.put("time",visitor.getTimingId());
            send(visitorJson, Const.HTTP_PROVIDER_CREATE_URL);
        }
    }

    @Override
    public void updateRecord(Visitor visitor) {
        if(!Const.BATCH_PROCESSING) {
            String log=visitor.getId()+":{lat"+visitor.getPosition().getLat()+";lon"+visitor.getPosition().getLon()+"}@"+visitor.getTimingId()+" Provider::Update";
            logger.info(log);
            JSONObject updateJSON = new JSONObject();
            updateJSON.put("id", visitor.getId());
            updateJSON.put("position", GHPointToJSON(visitor.getPosition()));
            updateJSON.put("time",visitor.getTimingId());
            send(updateJSON, Const.HTTP_PROVIDER_UPDATE_URL);
        }
    }

    @Override
    public void updateRecord(Visitor visitor, GHPoint correctedPosition) throws NotImplementedException{
        throw new NotImplementedException();
    }

    @Override
    public void finalizeRecord(Visitor visitor) {
        if(Const.BATCH_PROCESSING) {
            String log=visitor.getId()+":{lat"+visitor.getPosition().getLat()+";lon"+visitor.getPosition().getLon()+"}@"+visitor.getTimingId()+" Provider::Finalize";
            logger.info(log);
            JSONObject visitorStrategies = new JSONObject();
            visitorStrategies.put("goal", visitor.getGoalStrategy().getName());
            visitorStrategies.put("routing", visitor.getRoutingStrategy().getName());
            visitorStrategies.put("enjoying", visitor.getEnjoyingStrategy().getName());

            JSONObject positionMapJSON = new JSONObject();
            for (Map.Entry<Long, GHPoint> tPosition : visitor.getPositionMap().entrySet()) {
                positionMapJSON.put(tPosition.getKey().toString(), GHPointToJSON(tPosition.getValue()));
            }
            JSONObject visitorJson = new JSONObject();
            visitorJson.put("id", visitor.getId());
            visitorJson.put("strategies", visitorStrategies);
            visitorJson.put("home", GHPointToJSON(visitor.getHome()));
            visitorJson.put("interest_needed", visitor.getInterestNeeded());
            visitorJson.put("interest_reduce_per_time", visitor.getInterestReducePerTime());
            visitorJson.put("position_map", positionMapJSON);
            send(visitorJson, Const.HTTP_PROVIDER_FINALIZE_URL);
        }

    }

    @Override
    public void createTransmitterStations(List<TransmitterStation> stationList) throws NotImplementedException{
        throw new NotImplementedException();
    }

    @Override
    public void createCluster(Cluster rootCluster) throws NotImplementedException {
        throw new NotImplementedException();
    }

    private void send(JSONObject obj, String url){

        try {
            HttpClient httpClient = HttpClientPool.getClient();
            HttpPost request = new HttpPost(url);
            StringEntity params =new StringEntity(obj.toString());
            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            CloseableHttpResponse response = (CloseableHttpResponse) httpClient.execute(request);
            response.close();
        }catch (Exception ex) {
            logger.error("http error", ex);
        } finally {
        }
    }

    private JSONObject GHPointToJSON(GHPoint point){
        JSONObject pointJSON =new JSONObject();
        pointJSON.put("lat",point.getLat());
        pointJSON.put("lon",point.getLon());
        return pointJSON;
    }

}
