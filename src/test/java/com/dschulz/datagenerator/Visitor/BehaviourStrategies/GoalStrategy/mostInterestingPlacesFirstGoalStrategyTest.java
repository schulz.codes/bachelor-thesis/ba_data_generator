package com.dschulz.datagenerator.Visitor.BehaviourStrategies.GoalStrategy;

import com.dschulz.datagenerator.Routing.Hopper;
//import com.dschulz.datagenerator.Visitor.BehaviourStrategies.GoalStrategy.mostInterestingClusterFirstGoalStrategy;
import com.dschulz.datagenerator.TestClass;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by daniel on 08.05.16.
 */
public class mostInterestingPlacesFirstGoalStrategyTest  extends TestClass {

    @Before
    public void setUp() throws Exception {
        super.setUp();
    }

    @Test
    public void testFindGoal() throws Exception {

    }

    @Test
    public void testGetName() throws Exception {
        assertTrue(new mostInterestingPlacesFirstGoalStrategy().getName()=="MIPF");
    }
}