package com.dschulz.datagenerator.Place;

import com.dschulz.datagenerator.Cluster.DirectedEdge;
import com.dschulz.datagenerator.Const;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.graphhopper.util.shapes.GHPoint;
import com.dschulz.datagenerator.Math.MathDistributions;

import org.apache.log4j.Logger;
import org.jgrapht.WeightedGraph;
import org.jgrapht.graph.SimpleWeightedGraph;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by daniel on 08.04.16.
 */
public class Place implements Comparable<Place>{


    static final Logger logger = Logger.getLogger(Place.class);
    long Id;
    GHPoint Position;
    String Name;
    Double InterestFactor;

    protected Place(long id, GHPoint position,String name){
        Position=position;
        Name=name;
        Id= id;
        InterestFactor= MathDistributions.RandomInRange(Const.MIN_BUILDING_INTEREST, Const.MAX_BUILDING_INTEREST);
    }
    protected Place(long id, GHPoint position, String name, double interestFactor){
        Position=position;
        Name=name;
        Id=  id;
        InterestFactor= interestFactor;
    }

    //region Getter
    public GHPoint getPosition() {
        return Position;
    }

    public String getName() {
        return Name;
    }

    public Double getInterestFactor() {
        return InterestFactor;
    }

    public long getId() {
        return Id;
    }
    //endregion


    public boolean equals(Place place){
        if(place.Id==this.Id){
            return true;
        }else{
            return false;
        }
    }

    @Override
    public int compareTo(Place o) {
        return -1*InterestFactor.compareTo(o.getInterestFactor());
    }
}
