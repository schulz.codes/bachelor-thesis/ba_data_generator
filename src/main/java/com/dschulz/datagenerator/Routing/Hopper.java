package com.dschulz.datagenerator.Routing;

import com.graphhopper.GraphHopper;
import com.graphhopper.routing.util.EncodingManager;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by daniel on 08.04.16.
 */
public class Hopper extends  GraphHopper {
    private static final Hopper hopper = new Hopper();
    public static final String ghLoc = "./resources/ghosm";
    public static final String ghTestLoc="./resources/ghosm";
    public static final String testOSM="./resources/thueringen.osm.pbf";
    public static boolean didLoad=false;

    private Hopper(){
    }

    public boolean loadFromOSM(String OSMLocation, boolean forceDelete, boolean test){
        if(!didLoad) {
            this.setOSMFile(OSMLocation);
            // where to store graphhopper files?
            if (test) {
                this.setGraphHopperLocation(ghTestLoc);
            } else {
                this.setGraphHopperLocation(ghLoc);
                if (forceDelete) {
                    try {
                        FileUtils.deleteDirectory(new File(this.getGraphHopperLocation()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            this.setEncodingManager(new EncodingManager("foot,car,bike"));
            this.setCHWeightings(Arrays.asList("shortest", "fastest"));
            // now this can take minutes if it imports or a few seconds for loading
            // of course this is dependent on the area you import
            this.importOrLoad();
            didLoad = true;
            return true;
        }
        return false;
    }

    public boolean loadFromOSM(String OSMLocation, boolean forceDelete){
        if(!didLoad) {
            return this.loadFromOSM(OSMLocation, forceDelete, false);
        }else{
            return false;
        }
    }

    public static synchronized Hopper getInstance(){
       return hopper;
    }
}
