package com.dschulz.datagenerator.Cluster.Stategies;

import com.dschulz.datagenerator.Cluster.Cluster;
import com.dschulz.datagenerator.Cluster.Distance;
import com.dschulz.datagenerator.Routing.Hopper;
import com.graphhopper.GHRequest;
import com.graphhopper.GHResponse;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Collection;
import java.util.Locale;

/**
 * Created by daniel on 05.07.16.
 */
public class CalculatedLinkageStrategy implements LinkageStrategy {
    @Override
    public Distance calculateDistance(Collection<Distance> distances) {
       throw new NotImplementedException();
    }

    @Override
    public Distance calculateDistance(Cluster from, Cluster to) {
        GHRequest req = new GHRequest(from.getPosition(),to.getPosition()).
                setWeighting("fastest").
                setVehicle("foot").
                setLocale(Locale.GERMAN);
        GHResponse rsp = Hopper.getInstance().route(req);
        Double dst=rsp.getBest().getDistance();
        if(dst==null){
            return new HaversineLinkageStrategy().calculateDistance(from,to);
        }else {
            return new Distance(dst);
        }
    }
}
