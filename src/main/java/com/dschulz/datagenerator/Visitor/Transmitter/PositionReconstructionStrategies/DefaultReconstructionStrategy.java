package com.dschulz.datagenerator.Visitor.Transmitter.PositionReconstructionStrategies;

import com.dschulz.datagenerator.Const;
import com.dschulz.datagenerator.Math.Functions;
import com.dschulz.datagenerator.Math.MathDistributions;
import com.dschulz.datagenerator.Statistics.Enum.TransmitterReconstructionStrategy;
import com.dschulz.datagenerator.Statistics.Statistics;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStation;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.graphhopper.util.shapes.GHPoint;
import org.apache.log4j.Logger;

import java.util.*;

/**
 * Created by daniel on 15.06.16.
 */
public class DefaultReconstructionStrategy implements PositionReconstructionStrategy {

    private static final Logger logger = Logger.getLogger("Visitor");
    @Override
    public GHPoint getReconstructedPosition(Visitor visitor, TreeMap<Double, TransmitterStation> stationDistanceMap) {
        //logger.info("reconstruction of Position"+ visitor.getPosition().toString());
        long startTime= System.nanoTime();
        if(stationDistanceMap.size()==0){
            long endTime=System.nanoTime()-startTime;
            Statistics.getInstance().addReconstructionStrategyTime(this.getClass().getSimpleName(), TransmitterReconstructionStrategy.NO_STATION, endTime);
            return null;
        }
        List<GHPoint> intersectionPoints=new ArrayList<>();

        //returns the TransmitterStation with the highst Distance to the visitor;
        Double searchDistance=stationDistanceMap.lastEntry().getKey();


        while(intersectionPoints.size()<Const.COUNT_POSSIBLE_INTERSECTION_POINTS  && !(System.nanoTime()-startTime>Const.TRANSMITTER_RECONSTRUCTION_TIMEOUT)) {
            //logger.info(System.nanoTime()-startTime+"ns running"+(System.nanoTime()-startTime>Math.pow(10,9)));
            GHPoint possibleIntersectionPoint =MathDistributions.EqualInCircle(visitor.getPosition(),searchDistance);
            boolean notInAllStations = false;
            for (TransmitterStation station : stationDistanceMap.values()) {
                double tDistance=Functions.Distance(station.getPosition(), possibleIntersectionPoint)*1000;
                if (tDistance > station.getRadius()) {
                    notInAllStations = true;
                    break;
                }
            }
            if (!notInAllStations) {
                //logger.info("new possible Point "+possibleIntersectionPoint.toString());
                intersectionPoints.add(possibleIntersectionPoint);
            }
        }
        boolean aborted=false;
        if( System.nanoTime()-startTime>Const.TRANSMITTER_RECONSTRUCTION_TIMEOUT){
            //logger.info("Over Interval Time::::::::::::");
            if(intersectionPoints.size()==0){
                long endTime=System.nanoTime()-startTime;
                Statistics.getInstance().addReconstructionStrategyTime(this.getClass().getSimpleName(), TransmitterReconstructionStrategy.ABORTED_TIME, endTime);
                return null;
            }else{
                aborted=true;
            }
        }
        double latMid=0, lonMid=0;
        for(GHPoint point : intersectionPoints){
            latMid+=point.getLat();
            lonMid+=point.getLon();
        }

        GHPoint midPoint=new GHPoint(latMid/intersectionPoints.size(),lonMid/intersectionPoints.size());
        //logger.info("resulting Point "+midPoint.toString());
        long endTime=System.nanoTime()-startTime;
        Statistics.getInstance().addReconstructionStrategyTime(this.getClass().getSimpleName(), TransmitterReconstructionStrategy.POINTS_FOUND, Long.valueOf(intersectionPoints.size()));
        double dist=Functions.Distance(midPoint,visitor.getPosition());


        Double dbl =new Double(dist);
        try {
            if (dbl != null) {
                if (!dbl.isInfinite() && !dbl.isNaN()) {
                    Statistics.getInstance().addValueToStatistics("TRAJECTORY", (dbl * 1000));
                } else {
                    throw new Exception("Dbl is " + dbl.toString());
                }
            }else{
                throw new Exception("DBL is null");
            }
        }catch(Exception e){
            logger.error(e);
        }

        if(aborted) {
            Statistics.getInstance().addReconstructionStrategyTime(this.getClass().getSimpleName(), TransmitterReconstructionStrategy.FOUND_POINT_ABORTED_TIME, endTime);

        }else{
            Statistics.getInstance().addReconstructionStrategyTime(this.getClass().getSimpleName(), TransmitterReconstructionStrategy.FOUND_POINT_TIME, endTime);

        }
        return midPoint;

    }
}
