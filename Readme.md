#Building (jar)
1. call `mvn install clean` in Terminal
2. the jar should be builded in `target/datagenerator.jar`

#Running
##Configuration.properties
For a successful running you have to provide a *.properties-File to the jar
an example config.properties-File looks like:

```java
#Sat May 07 09:12:31 CEST 2016

#which DataProvider should be use to send the Data?
#available Options are:
#   MongoDataProvider
#   HttpDataProvider
VISITOR_DATA_PROVIDER=HttpDataProvider

#should the DataProvider send Data during the Simulation of an User
#or the complete Dataset after the Visitor is back Home
UPDATE_DURING_SIMULATION=false

#should the Simulation wait VISITOR_SLEEPTIME until the next Entry is generated or recall itself
BATCH_PROCESSING=true

#Describes the time a cluster waits until he tries to create new Visitors
#(Visitor Creation is based on Cluster Interest/Size)
SIMULATION_CLUSTER_SLEEPTIME=1000.0

#In which radius around Cluster CenterPoint should a new Visitor appear? (in meters)
SIMULATION_CLUSTER_PLACES_DISTANCE=1500.0

#How much interest a Cluster has to generate until a new Visitor appears?
SIMULATION_CLUSTER_INTEREST_THRESHOLD=100.0

#How Much interest has a Place minimum.
MIN_BUILDING_INTEREST=15.0
#How Much interest has a Place maximum.
#(Cluster Interest is the sum of Interest from all Places inside the Cluster)
MAX_BUILDING_INTEREST=30.0

#How many Visitors are allowed to walk around the same time
MAX_VISITORS_SAME_TIME=5
#how many visitors should be simulated until Simulation stops
MAX_VISITORS_TOTAL=20
#how long should a Visitor sleep until he sends Data/does his next Visitor Step (in Milliseconds)
VISITOR_SLEEPTIME=5000

#Distance which is taken by a Visitor per Visitorstep in KM
VISITOR_DISTANCE_PER_SIMULATION_STEP=0.001

#how much does the Steplength vary between different Visitors in KM
VISITOR_VARIANCE_PER_SIMULATION_STEP=0.0002

#how much does the Steplength of a single Visitor vary in KM
VISITOR_MEAN_VARIANCE_PER_SIMULATION_STEP=0.0001

#How much Interest can a Visitor reduce minimum per Visitor Step
MIN_INTEREST_REDUCE_PER_TIME=0.1
#How much Interest can a Visitor reduce maximum per Visitor Step
MAX_INTEREST_REDUCE_PER_TIME=0.5
#How much Interest does a Visitor have minimum on Instantiation
MIN_INTEREST_NEED=60.0
#How much Interest does a Visitor have maximum on Instantiation
MAX_INTEREST_NEED=120.0


#MongoSettings - should be self-explanatory
MONGO_DB_NAME=leafletDB
MONGO_COLLECTION_VISITOR=visitors
MONGO_ADDRESS=localhost
MONGO_PORT=27017

#HttpDataProviderSettings
HTTP_PROVIDER_FINALIZE_URL=localhost:8888
HTTP_PROVIDER_UPDATE_URL=localhost:8888
HTTP_PROVIDER_CREATE_URL=localhost:8888
```

after you configured the Config File to your needs you can run the jar with thr following command:

`java -jar target/datagenerator -run ./resources/thueringen.osm.pbf false ./config.properties`

Please use the thueringen.osm.pbf because the Places-Positions still reference to lat/lon in there. 

##HttpDataProvider JSON Format
There are two different kinds of getting data from the Datagenerator first is to do the Simulation in realtime. In this mode
each Visitor will send updates in Interval Const.VISITOR_SLEEPTIME until he is back home. The initial request will provide
additional information to the Visitors Behaviour.

To use Realmode set UPDATE_DURING_SIMULATION=true and BATCH_PROCESSING=false
```json
{
    "interest_needed": 111.30718860983242,
    "strategies": {
        "routing": "shortestRouteRoutingStrategy",
        "goal": "interestDistanceFactorGoalStrategy",
        "enjoying": "DefaultEnjoyingStrategy"
    },
    "id": 0,
    "interest_reduce_per_time": 0.3495478892220941,
    "position": {
        "lon": 10.93839748066062,
        "lat": 50.68204609339042
    },
    "time": 2335,
    "home": {
        "lon": 10.93839748066062,
        "lat": 50.68204609339042
    }
}
```

The Update Requests just includes the current id, position and Simulation Time since start in Milliseconds

```json
{
    "id": 0,
    "position": {
        "lon": 10.938398055556368,
        "lat": 50.68203883534232
    },
    "time": 7335
}
```

The Second kind you can get your data from the Datagenerator is to use it in BatchMode and get the whole Visitor Movement in
one request. The PositionMap includes all Positions of a User with the time attribute as key.

To use Batchmode set UPDATE_DURING_SIMULATION=false and BATCH_PROCESSING=true
```json
{
    "interest_needed": 0,
    "strategies": {
        "routing": "fastestRouteRoutingStrategy",
        "goal": "nearestPlacesFirstGoalStrategy",
        "enjoying": "DefaultEnjoyingStrategy"
    },
    "position_map": {
        "2035": {
            "lon": 10.938859789196993,
            "lat": 50.68196376447299
        },
        "7035": {
            "lon": 10.93607998684201,
            "lat": 50.682056323340305
        },
        ...
        "12035": {
            "lon": 10.933444334569074,
            "lat": 50.682598367689515
        },
        "6172222": {
            "lon": 10.938859789196993,
            "lat": 50.68196376447299
        },
    },
    "id": 0,
    "interest_reduce_per_time": 0.11835384548987844,
    "home": {
        "lon": 10.938859789196993,
        "lat": 50.68196376447299
    }
}
```

##Own PlaceDefinition
You can provide an Place Definition by your own. Simply run the jar with the following parameters:
`java -jar target/datagenerator -run ./resources/thueringen.osm.pbf false ./config.properties ./places.json`

your Json-File should have the following Structure:
```json
[
  {
    "name": "Humboldtbau",
    "position": {
      "lat": 50.682903,
      "lon": 10.937857
    },
    "interest": 20.345
  },
  {
    "name": "Helmholtz",
    "position": {
      "lat": 50.681765,
      "lon": 10.939121
    }
  }
]
```
interest is an optional attribute to give a place a fixed interest Factor for each simulation otherwise the Interest will
change each Simulation depending on MIN_BUILDING_INTEREST and MAX_BUILDING_INTEREST in your configuration.