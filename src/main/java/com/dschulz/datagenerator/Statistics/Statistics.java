package com.dschulz.datagenerator.Statistics;

import com.dschulz.datagenerator.App;
import com.dschulz.datagenerator.Const;
import com.dschulz.datagenerator.Math.Functions;
import com.dschulz.datagenerator.Place.*;
import com.dschulz.datagenerator.Statistics.Enum.*;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.log4j.Logger;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by daniel on 15.06.16.
 */
public class Statistics {
    private static final Logger logger = Logger.getLogger("Statistics");
    //region Singleton
    private static final class InstanceHolder {
        static final Statistics INSTANCE = new Statistics();
    }

    private Statistics() {
    }

    public static Statistics getInstance () {
        return Statistics.InstanceHolder.INSTANCE;
    }
    //endregion
    HashMap<Long, HashMap<String,Double>> trajMap=new HashMap<>();
    HashMap<String,HashMap<String,Object>> overallVisitedPlaces= new HashMap<>();

    private TreeMap<String, ArrayList<Long>> reconstructionStrategy = new TreeMap<>();
    private TreeMap<String, ArrayList<Long>> distributionStrategy = new TreeMap<>();
    private TreeMap<String, ArrayList<Long>> enjoyingStrategy = new TreeMap<>();
    private TreeMap<String, ArrayList<Long>> goalStrategy = new TreeMap<>();
    private TreeMap<String, ArrayList<Long>> routingStrategy = new TreeMap<>();
    private TreeMap<String, ArrayList<Long>> cluster = new TreeMap<>();
    private TreeMap<String,ArrayList<Double>> allList= new TreeMap<>();
    private HashMap<String,ArrayList<Object>> visitorStatistics= new HashMap<>();

    private String buildStrategyKey(Visitor visitor){
        String key="";
        key+="R:"+visitor.getRoutingStrategy().getName();
        key+="G:"+visitor.getGoalStrategy().getName();
        return key;
    }
    //region durch
    public void addVisitorStatistics(Visitor visitor){
        String stratKey=buildStrategyKey(visitor);
        addValueToStatistics("DW"+stratKey,visitor.getDistanceWalked());
        addValueToStatistics("STI"+stratKey,visitor.getTimingId()-visitor.getStartTimeOffset());
        addValueToStatistics("SSTE"+stratKey,visitor.getVisitorStep());
        addValueToStatistics("WSTE"+stratKey,visitor.getWalkingSteps());
        addValueToStatistics("ESTE"+stratKey,visitor.getEnjoyingSteps());
        addValueToStatistics("PVIS"+stratKey,visitor.getVisitedPlaces().size());
    }

    public void addValueToStatistics(String strategyKey, Object value){
        ArrayList<Double> visitorStrategyMap=allList.get(strategyKey);
        if(visitorStrategyMap==null){
            allList.put(strategyKey,new ArrayList<Double>());
            visitorStrategyMap=allList.get(strategyKey);
        }
        visitorStrategyMap.add(Double.parseDouble(value.toString()));
    }

    public synchronized void addReconstructionStrategyTime(String strategyClass, TransmitterReconstructionStrategy state, Long timeInNs){
        String key=strategyClass+""+state.name();
        TreeMap<String, ArrayList<Double>> treeMap=allList;
        //FIX if no Station
        String key2=strategyClass+""+TransmitterReconstructionStrategy.NO_STATION.name();
        if(treeMap.get(key2)==null){
            treeMap.put(key2,new ArrayList<Double>());
        }
        String key3=strategyClass+""+TransmitterReconstructionStrategy.ABORTED_TIME.name();
        if(treeMap.get(key3)==null){
            treeMap.put(key3,new ArrayList<Double>());
        }
        key3=strategyClass+""+TransmitterReconstructionStrategy.POINTS_FOUND.name();
        if(treeMap.get(key3)==null){
            treeMap.put(key3,new ArrayList<Double>());
        }
        key3=strategyClass+""+TransmitterReconstructionStrategy.FOUND_POINT_ABORTED_TIME.name();
        if(treeMap.get(key3)==null){
            treeMap.put(key3,new ArrayList<Double>());
        }
        key3=strategyClass+""+TransmitterReconstructionStrategy.FOUND_POINT_TIME.name();
        if(treeMap.get(key3)==null){
            treeMap.put(key3,new ArrayList<Double>());
        }
        if(treeMap.get(key)==null){
            treeMap.put(key,new ArrayList<Double>());
        }
        treeMap.get(key).add(Double.parseDouble(timeInNs.toString()));
    }

    public synchronized void addTransmitterDistributionStrategyTime(String strategyClass, TransmitterDistributionStrategy state, Long timeInNs){
        String key=strategyClass+""+state.name();
        TreeMap<String, ArrayList<Double>> treeMap=allList;
        if(treeMap.get(key)==null){
            treeMap.put(key,new ArrayList<Double>());
        }
        treeMap.get(key).add(Double.parseDouble(timeInNs.toString()));
    }

    public synchronized  void addEnjoyingStrategyTime(String strategyClass, EnjoyingStrategy state, Long timeInNs){
        String key=strategyClass+""+state.name();
        TreeMap<String, ArrayList<Double>> treeMap=allList;
        if(treeMap.get(key)==null){
            treeMap.put(key,new ArrayList<Double>());
        }
        treeMap.get(key).add(Double.parseDouble(timeInNs.toString()));
    }

    public synchronized  void addGoalStrategyTime(String strategyClass, GoalStrategy state, Long timeInNs){
        String key=strategyClass+""+state.name();
        TreeMap<String, ArrayList<Double>> treeMap=allList;
        if(treeMap.get(key)==null){
            treeMap.put(key,new ArrayList<Double>());
        }
        treeMap.get(key).add(Double.parseDouble(timeInNs.toString()));
    }

    public synchronized  void addRoutingStrategyTime(String strategyClass, RoutingStrategy state, Long timeInNs){
        String key=strategyClass+""+state.name();
        TreeMap<String, ArrayList<Double>> treeMap=allList;
        if(treeMap.get(key)==null){
            treeMap.put(key,new ArrayList<Double>());
        }
        treeMap.get(key).add(Double.parseDouble(timeInNs.toString()));
    }

    public synchronized  void addClusterTime(String strategyClass, Cluster state, Long timeInNs){
        String key=strategyClass+""+state.name();
        TreeMap<String, ArrayList<Double>> treeMap=allList;
        if(treeMap.get(key)==null){
            treeMap.put(key,new ArrayList<Double>());
        }
        treeMap.get(key).add(Double.parseDouble(timeInNs.toString()));
    }
    //endregion

    public void displayStatistics(){
        displayVisitorStatistics();
    }


    /*public void addTrajectory(Visitor visitor){

        Double minDist = Double.MAX_VALUE;
        Double maxDist = Double.MIN_VALUE;
        Double avg = 0.0;
        for (Map.Entry<Long, GHPoint> synSet : visitor.getPositionMap().entrySet()) {
            GHPoint synEntry = visitor.getCorrectedPositionMap().get(synSet.getKey());
            if(synEntry!=null) {
                Double dist = Functions.Distance(synEntry, synSet.getValue());
                if (dist > maxDist) {
                    maxDist = dist;
                }
                if (dist < minDist) {
                    minDist = dist;
                }
                avg += dist;
            }
        }
        avg = avg / visitor.getPositionMap().size();
        Double variance=0.0;
        for (Map.Entry<Long, GHPoint> synSet : visitor.getPositionMap().entrySet()) {
            GHPoint synEntry = visitor.getCorrectedPositionMap().get(synSet.getKey());
            if(synEntry!=null) {
                Double dist = Functions.Distance(synEntry, synSet.getValue());
                variance+=(dist-avg)*(dist-avg);
            }
        }
        addValueToStatistics("TRAJECTORY_VARIANCE",variance/visitor.getCorrectedPositionMap().size());
        addValueToStatistics("TRAJECTORY_MIN",minDist);
        addValueToStatistics("TRAJECTORY_MAX",maxDist);
        addValueToStatistics("TRAJECTORY_AVG",avg);
    }*/

    public void addVisitedPlaces(Visitor visitor){
        for(com.dschulz.datagenerator.Place.Place place: visitor.getVisitedPlaces()){
            if(overallVisitedPlaces.get(place.getName())==null){
                overallVisitedPlaces.put(place.getName(),new HashMap<String, Object>());

            }
            if(overallVisitedPlaces.get(place.getName()).get("visits")==null){
                overallVisitedPlaces.get(place.getName()).put("visits",1);
            }else{
                overallVisitedPlaces.get(place.getName()).put("visits",Long.parseLong(overallVisitedPlaces.get(place.getName()).get("visits")+"")+1);
            }
            if(overallVisitedPlaces.get(place.getName()).get("interest")==null){
                overallVisitedPlaces.get(place.getName()).put("interest",place.getInterestFactor());
            }
            if(overallVisitedPlaces.get(place.getName()).get("distToMainCluster")==null){
                overallVisitedPlaces.get(place.getName()).put("distToMainCluster",Functions.Distance(PlaceFactory.getInstance().getClustered().getPosition(),place.getPosition()));
            }
        }
    }

    private TreeMap<String,ArrayList<Double>> statistics(TreeMap<String,ArrayList<Double>> list,int quantil){
        TreeMap<String,ArrayList<Double>> returnList= new TreeMap<>();
        for(Map.Entry<String,ArrayList<Double>> objList:list.entrySet()) {
            ArrayList<Double> values = new ArrayList<>();
            Double minValue = Double.MAX_VALUE;
            Double maxValue = Double.MIN_VALUE;
            ArrayList<Double> valueList = objList.getValue();
            //Collections.sort(valueList);
            Double avg = 0.0;
            for (Double dbl : valueList) {
                try {
                    if (dbl > maxValue) {
                        maxValue = dbl;
                    }
                    if (dbl < minValue) {
                        minValue = dbl;
                    }
                    avg += dbl;
                }catch(NullPointerException e){
                    //logger.error("in "+objList.getKey(),e);
                }
            }
            avg = avg / valueList.size();
            values.add(new Double(valueList.size()));
            values.add(maxValue);
            values.add(minValue);
            values.add(avg);
            //Double maxMinDiff=maxValue-minValue;
            //Double qunatilSize= maxMinDiff/quantil;
            Double variance = 0.0;
            //Double inQuantil=0.0;
            TreeMap<Double, Integer> qMap = new TreeMap<>();

            /*for(int i= 1; i<=quantil;i++){
                qMap.put(minValue+i*qunatilSize,0);
            }
            Double prevKey=minValue;*/
            for (Double dbl2 : valueList) {
                try {
                    variance += (dbl2 - avg) * (dbl2 - avg);
                }catch(NullPointerException e){
                    //logger.error("in "+objList.getKey(),e);
                }
            }

               /*for(Map.Entry<Double,Integer> qEntry:qMap.entrySet()){
                    if(dbl<=qEntry.getKey() && dbl>=prevKey){
                        qMap.put(qEntry.getKey(),qEntry.getValue()+1);
                        break;
                    }
                   prevKey=qEntry.getKey();
               }*/
            values.add(variance / valueList.size());
            for (Integer qCount : qMap.values()) {
                values.add(new Double(qCount));
            }
            returnList.put(objList.getKey(), values);

        }
        return returnList;

    }
    public void displayVisitorStatistics(){
        long simtime=System.currentTimeMillis()-App.getSimulationStart();

        logger.info("Simulation took:"+simtime+" ms");
        logger.info("Total time:"+(simtime+App.getSetupTime())+" ms");
        logger.info("preparing Stats");
        ArrayList<Object> allStats=new ArrayList<>();

        addValueToStatistics("SimulationTime", System.currentTimeMillis()-App.getSimulationStart());
        HashMap<String,Object> timeMap=new HashMap<>();
        timeMap.put("SIMULATION_TIME",System.currentTimeMillis()-App.getSimulationStart());
        allStats.add(timeMap);
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        try {
            ArrayList<String> params = new ArrayList<>();
            params.add("Count");
            params.add("Max");
            params.add("Min");
            params.add("Avg");
            params.add("variance");
            int quantilSize=20;
            for(int i=0; i<quantilSize;i++){
                params.add("Quantil "+i);
            }

            CSVUtils.toCSV("Statistics/"+Const.PROPERTY_FILE_NAME+""+dateFormat.format(date)+".csv",statistics(allList,quantilSize),params);
        } catch (IOException e) {
            e.printStackTrace();
        }
        /*allStats.add(timeMap);
        allStats.add(reconstructionStrategy);
        allStats.add(distributionStrategy);
        allStats.add(enjoyingStrategy);
        allStats.add(goalStrategy);
        allStats.add(routingStrategy);
        allStats.add(cluster);
        allStats.add(visitorStatistics);

        allStats.add(visitorStatistics);*/
        HashMap<String,Object> placesyMap=new HashMap<>();
        placesyMap.put("PLACES",overallVisitedPlaces);
        allStats.add(placesyMap);


        /*HashMap<String,Object> trajectoryMap=new HashMap<>();
        trajectoryMap.put("TRAJECTORY",trajMap);
        allStats.add(trajectoryMap);*/
        GsonBuilder gsonBuilder  = new GsonBuilder();
        // Allowing the serialization of static fields
        gsonBuilder.excludeFieldsWithModifiers(java.lang.reflect.Modifier.TRANSIENT).excludeFieldsWithoutExposeAnnotation();
        // Creates a Gson instance based on the current configuration

        HashMap<String,Const> constMap= new HashMap<>();
        constMap.put("Const",new Const());
        allStats.add(constMap);

        Gson gson = gsonBuilder.create();
        try (Writer writer = new FileWriter("Statistics/"+Const.PROPERTY_FILE_NAME+" "+dateFormat.format(date)+" stat.json")) {
            //logger.info(gson.toJson(allStats));
            gson.toJson(allStats, writer);
            //logger.info("Json written to statistics.json");
        } catch (IOException e) {
            logger.info(gson.toJson(allStats));
            e.printStackTrace();
        }


    }

}
