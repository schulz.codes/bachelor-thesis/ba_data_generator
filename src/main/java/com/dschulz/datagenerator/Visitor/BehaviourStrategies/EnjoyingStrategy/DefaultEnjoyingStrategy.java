package com.dschulz.datagenerator.Visitor.BehaviourStrategies.EnjoyingStrategy;

import com.dschulz.datagenerator.Place.Place;
import com.dschulz.datagenerator.Place.PlaceFactory;
import com.dschulz.datagenerator.Statistics.Enum.EnjoyingStrategy;
import com.dschulz.datagenerator.Statistics.Statistics;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.graphhopper.util.shapes.GHPoint;

/**
 * Created by daniel on 19.04.16.
 */
public class DefaultEnjoyingStrategy implements  VisitorEnjoyStrategy {
    long timeStayed=0;
    double time=0;
    @Override
    public boolean leave(Visitor visitor, GHPoint placePosition) {
        long startTime= System.nanoTime();
        if(visitor.getInterestNeeded()==0){
            return true;
        }
        if(time==0) {
            Place place= PlaceFactory.getInstance().getPlaceToPosition(placePosition);
            double subjectiveInterest= place.getInterestFactor()*visitor.getPlaceAttachement();
            if (subjectiveInterest >= visitor.getInterestNeeded()) {
                time = Math.ceil((visitor.getInterestNeeded()+visitor.getInterestReducePerTime()) / (visitor.getInterestReducePerTime()));
            } else {
                time = Math.ceil(subjectiveInterest / visitor.getInterestReducePerTime());
            }
        }
        timeStayed++;
        long endTime=System.nanoTime()-startTime;
        Statistics.getInstance().addEnjoyingStrategyTime(this.getClass().getSimpleName(), EnjoyingStrategy.LEAVE_TIME,endTime);
        if(timeStayed>time){
            time=0;
            timeStayed=0;
            return true;
        }else{
            return false;
        }
    }

    @Override
    public double getInterestReduce(Visitor visitor, GHPoint place) {
        return visitor.getInterestReducePerTime();
    }

    @Override
    public String getName() {
        return this.getClass().getSimpleName();
    }
}
