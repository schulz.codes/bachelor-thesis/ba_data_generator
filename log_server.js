// Service to receive Log Messages
var Port = 8888;

var tHttp = require("http");
var tQueryString = require('querystring');

function processPost(request, response) {
	var body = '';
	if (request.method == 'POST') {
		request.on('data', function(data) {
			body += data;
			console.log("REQUEST START");
		});

		request.on('end', function() {
			console.log(body);
			console.log("REQUEST END");
		});
	}
	else {
		response.writeHead(405, {'Content-Type': 'text/plain'});
		response.end();
    }
}

tHttp.createServer(function(request, response) {
	if (request.method == 'POST') {
		processPost(request, response);
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.write('Alive and running...');
		response.end();
	}
	else {
		response.writeHead(200, {"Content-Type": "text/plain"});
		response.write('Alive and running...');
		response.end();
	}
}).listen(Port);

console.log('Log Server started at Port ' + Port);