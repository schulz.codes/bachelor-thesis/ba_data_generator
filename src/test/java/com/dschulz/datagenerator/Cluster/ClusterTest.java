/*******************************************************************************
 * Copyright 2013 Lars Behnke
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.dschulz.datagenerator.Cluster;

import com.dschulz.datagenerator.Cluster.Algorithm.ClusteringAlgorithm;
import com.dschulz.datagenerator.Cluster.Algorithm.GraphClusteringAlgorithm;
import com.dschulz.datagenerator.Cluster.Stategies.HaversineLinkageStrategy;
import com.dschulz.datagenerator.Place.Place;
import com.dschulz.datagenerator.Place.PlaceFactory;
import com.dschulz.datagenerator.Routing.Hopper;
import com.dschulz.datagenerator.SampleData;
import com.dschulz.datagenerator.TestClass;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class ClusterTest extends TestClass {

    private Cluster cluster;


    @Before
    public void setup() throws Exception{
        super.setUp();
        ClusteringAlgorithm alg = new GraphClusteringAlgorithm();
        cluster = alg.performClustering(SampleData.GRAPH(),
                new HaversineLinkageStrategy());
    }
    
    @Test
    public void tesetCountLeafs() throws Exception {
        int leafs = cluster.countLeafs();
        assertEquals(7, leafs);
    }


    @Test
    public void testGetPlace() throws Exception {
            testGetPlaceRecursion(cluster);
    }
    private void testGetPlaceRecursion(Cluster cluster){
        if(cluster.getChildren().size()>0) {
            assertEquals(null, cluster.getPlace());
            for (Cluster child : cluster.getChildren()) {
                testGetPlaceRecursion(child);
            }
        }else{
            assertNotEquals(null,cluster.getPlace());
        }
    }

}
