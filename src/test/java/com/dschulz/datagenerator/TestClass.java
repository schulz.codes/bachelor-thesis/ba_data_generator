package com.dschulz.datagenerator;

import com.dschulz.datagenerator.Cluster.Cluster;
import com.dschulz.datagenerator.Cluster.DirectedEdge;
import com.dschulz.datagenerator.Place.Place;
import com.dschulz.datagenerator.Place.PlaceFactory;
import com.dschulz.datagenerator.Place.PlaceJson;
import com.dschulz.datagenerator.Routing.Hopper;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStationFactory;
import com.dschulz.datagenerator.Visitor.VisitorFactory;
import com.graphhopper.util.shapes.GHPoint;
import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.jgrapht.WeightedGraph;
import org.junit.Before;

import java.io.File;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;

/**
 * Created by daniel on 08.08.16.
 */
public abstract class TestClass {
    protected File realDUMMYPropertiesFile = new File("./src/test/resources/realDUMMY.properties");

    protected static Cluster cluster=null;
    protected static WeightedGraph<Place, DirectedEdge> graph=null;
    protected HashMap<GHPoint, Place> places= null;
    protected List<PlaceJson> placejson =null;

    @Before
    public void setUp() throws Exception {
        BasicConfigurator.configure();
        App.propertiesExist(realDUMMYPropertiesFile);
        Hopper.getInstance().loadFromOSM(Hopper.testOSM, false, true);

        cluster= SampleData.CLUSTER();
        graph = SampleData.GRAPH();
        places= SampleData.PLACES();
        placejson= SampleData.PLACEJSON();


        Field pFInstance = PlaceFactory.class.getDeclaredField("instance");
        pFInstance.setAccessible(true);
        pFInstance.set(null, null);
        PlaceFactory.getInstance().InitPlaces(placejson);

        Field tSFInstance=TransmitterStationFactory.class.getDeclaredField("instance");
        tSFInstance.setAccessible(true);
        tSFInstance.set(null,null);


        Field vFInstance= VisitorFactory.class.getDeclaredField("instance");
        vFInstance.setAccessible(true);
        vFInstance.set(null, null);
    }
}
