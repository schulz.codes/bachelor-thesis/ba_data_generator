#!/bin/bash
FILES=./propertyFiles/*
for f in $FILES
do
  echo "Processing $f file..."
  # take action on each file. $f store current file name
  java -jar target/datagenerator.jar -run ./resources/thueringen.osm.pbf false "$f" ./places_kus.json 0
  echo $f
done
