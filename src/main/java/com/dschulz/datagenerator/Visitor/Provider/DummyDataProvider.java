package com.dschulz.datagenerator.Visitor.Provider;

import com.dschulz.datagenerator.Cluster.Cluster;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStation;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.graphhopper.util.shapes.GHPoint;
import org.apache.log4j.Logger;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;


import java.util.List;


/**
 * Created by daniel on 08.05.16.
 */
public class DummyDataProvider implements DataProvider {
    static final Logger logger = Logger.getLogger("DataProvider");
    @Override
    public void createRecord(Visitor visitor) {
        String log=visitor.getId()+":{lat"+visitor.getPosition().getLat()+";lon"+visitor.getPosition().getLon()+"}@"+visitor.getTimingId()+" Provider::Create";
        //logger.info(log);
    }

    @Override
    public void updateRecord(Visitor visitor) {
        String log=visitor.getId()+":{lat"+visitor.getPosition().getLat()+";lon"+visitor.getPosition().getLon()+"}@"+visitor.getTimingId()+" Provider::Update";

        //logger.info(log);
    }
    @Override
    public void updateRecord(Visitor visitor, GHPoint correctedPosition) {
        String log=visitor.getId()+":{lat"+visitor.getPosition().getLat()+"/c:"+correctedPosition.getLat()+";lon"+visitor.getPosition().getLon()+"/c:"+correctedPosition.getLon()+"}@"+visitor.getTimingId()+" Provider::Update";

        //logger.info(log);
    }

    @Override
    public void finalizeRecord(Visitor visitor) {

        String log=visitor.getId()+":{lat"+visitor.getPosition().getLat()+";lon"+visitor.getPosition().getLon()+"}@"+visitor.getTimingId()+" Provider::Finalize";
        //logger.info(log);
    }

    @Override
    public void createTransmitterStations(List<TransmitterStation> stationList) throws NotImplementedException {
    }

    @Override
    public void createCluster(Cluster rootCluster) throws NotImplementedException {
    }

}
