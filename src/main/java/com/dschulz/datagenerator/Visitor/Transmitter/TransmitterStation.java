package com.dschulz.datagenerator.Visitor.Transmitter;

import com.graphhopper.util.shapes.GHPoint;

/**
 * Created by daniel on 15.06.16.
 */
public class TransmitterStation {
    GHPoint position;
    double radius;

    public TransmitterStation(GHPoint position, double radius){
        this.position=position;
        this.radius=radius;
    }

    public GHPoint getPosition() {
        return position;
    }

    public double getRadius() {
        return radius;
    }


}
