package com.dschulz.datagenerator;

/**
 * Created by daniel on 08.04.16.
 */

import com.dschulz.datagenerator.Visitor.Provider.DataProvider;
import com.dschulz.datagenerator.Visitor.Provider.MongoDataProvider;
import com.dschulz.datagenerator.Visitor.Transmitter.PositionReconstructionStrategies.DefaultReconstructionStrategy;
import com.dschulz.datagenerator.Visitor.Transmitter.PositionReconstructionStrategies.PositionReconstructionStrategy;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterDistributionStrategies.CircleDistributionStrategy;
import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterDistributionStrategies.TransmitterStationDistributionStrategy;
import com.google.gson.annotations.Expose;

import java.lang.*;

public final class Const {
    //Alle Zeitwerte werden in Millisekunden angegeben
    //Alle Entfernungen in KM

    @Expose public static String PROPERTY_FILE_NAME="";

    public static Class<? extends DataProvider> VISITOR_DATA_PROVIDER= new MongoDataProvider().getClass();
    @Expose public static boolean UPDATE_DURING_SIMULATION=false;
    @Expose public static boolean BATCH_PROCESSING=true;

    @Expose public static boolean TEST=false;



    //region Place/Cluster-Constants
    @Expose public static double MIN_BUILDING_INTEREST=15;
    @Expose public static double MAX_BUILDING_INTEREST=30;

    @Expose public static double SIMULATION_CLUSTER_SLEEPTIME=1000;// wie lange soll ein Cluster warten bis er versucht neue Besucher zu generiern
    @Expose public static double SIMULATION_CLUSTER_INTEREST_THRESHOLD=100;//Wieviel Interesse muss ein CLuster generieren, dass ein Neuer Besucher innerhalb erzeugt wird
    @Expose public static double SIMULATION_CLUSTER_PLACES_DISTANCE= 1500; // wie Groß soll der Durchmesser sein in dem ein Nutzer innerhalb eines Ortes generiert wird.
    //endregion

    //region VisitorConstants
    @Expose public static int VISITOR_SLEEPTIME =10;// wie lange soll ein besucher pro schritt benötigen
    @Expose public static int MAX_VISITORS_SAME_TIME =5; //wie viele Nutzer dürfen gleichzeitig aggieren?
    @Expose public static int MAX_VISITORS_TOTAL=20; //wie viele Nutzer bis Simulation beendet
    @Expose public static int VISITOR_COUNT_UNDER_100=1; //TUHttpProvider Wie viele Nutzer mit ID <100

    @Expose public static double VISITOR_DISTANCE_PER_SIMULATION_STEP=0.001;//IN KM = Distanz der Pro Besucher Pro Schritt zurück gelegt wird
    @Expose public static double VISITOR_VARIANCE_PER_SIMULATION_STEP=0.0002;//IN KM = Varianz von Schrittlänge bei unterschiedlichen Besuchern
    @Expose public static double VISITOR_MEAN_VARIANCE_PER_SIMULATION_STEP=0.0001;//IN KM Varianz pro Schritt eines Nutzers
    @Expose public static double MIN_INTEREST_NEED=60;
    @Expose public static double MAX_INTEREST_NEED=120;
    @Expose public static double MIN_INTEREST_REDUCE_PER_TIME=0.1;//TIME=Seconds
    @Expose public static double MAX_INTEREST_REDUCE_PER_TIME=0.5;//TIME=Seconds
    @Expose public static final double MEAN_PLACE_ATTACHEMENT=1;
    @Expose public static final double VARIANCE_PLACE_ATTACHEMENT=0.5;
    //endregion

    //region TransmitterConstants
    @Expose public static boolean TRANSMITTER_STATIONS=true;
    @Expose public static int TRANSMITTER_STEPS_BETWEEN=25;
    @Expose public static double TRANSMITTER_MIN_RADIUS_IN_METER=100;
    @Expose public static double TRANSMITTER_MAX_RADIUS_IN_METER=400;
    @Expose public static double TRANSMITTER_FILL_FACTOR=2;
    @Expose public static int COUNT_POSSIBLE_INTERSECTION_POINTS=10;
    @Expose public static int TRANSMITTER_RECONSTRUCTION_TIMEOUT=100000000;
    public static Class<? extends TransmitterStationDistributionStrategy> TRANSMITTER_STATION_DISTRIBUTION_STRATEGY=new CircleDistributionStrategy().getClass();
    public static Class<? extends PositionReconstructionStrategy> TRANSMITTER_POSITION_RECONSTRUCTION_STRATEGY=new DefaultReconstructionStrategy().getClass();


    //region Mongo-DataProvisioner
    public static String MONGO_COLLECTION_VISITOR ="visitors";
    public static String MONGO_DB_NAME = "leafletDB";
    public static String MONGO_ADDRESS="localhost";
    public static int MONGO_PORT=27017;
    //endregion


    //region HTTP-DataProvisioner
    public static String HTTP_PROVIDER_FINALIZE_URL="localhost:8888";
    public static String HTTP_PROVIDER_UPDATE_URL="localhost:8888";
    public static String HTTP_PROVIDER_CREATE_URL="localhost:8888";

    //endregion


    //region Lat/Long Constants
    public static final double EARTH_RADIUS=6378.1;
    public static final double LAT_IN_KM=110.574;
    public static final double KM_IN_LAT=1/LAT_IN_KM;

    public static double LONG_IN_KM(double latitude){
        return 111.320* Math.cos(latitude);
    }
    public static double KM_IN_LONG(double latitude){
        return 1/LONG_IN_KM(latitude);
    }
    //endregion

    public Const(){
    }
}
