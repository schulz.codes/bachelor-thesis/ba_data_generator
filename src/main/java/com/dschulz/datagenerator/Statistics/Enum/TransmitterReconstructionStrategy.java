package com.dschulz.datagenerator.Statistics.Enum;

/**
 * Created by daniel on 21.07.16.
 */
public enum TransmitterReconstructionStrategy {
    ABORTED_TIME, FOUND_POINT_TIME, FOUND_POINT_ABORTED_TIME, POINTS_FOUND,NO_STATION
}
