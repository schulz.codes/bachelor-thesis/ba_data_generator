package com.dschulz.datagenerator.Visitor.Transmitter;

import com.dschulz.datagenerator.Const;
import com.dschulz.datagenerator.Visitor.Transmitter.PositionReconstructionStrategies.PositionReconstructionStrategy;
import com.dschulz.datagenerator.Visitor.Visitor;
import com.graphhopper.util.shapes.GHPoint;

import java.util.TreeMap;

/**
 * Created by daniel on 14.06.16.
 */
public class TransmitterLogger implements PositionReconstructionStrategy {


    // Innere private Klasse, die erst beim Zugriff durch die umgebende Klasse initialisiert wird
    private static final class InstanceHolder {
        // Die Initialisierung von Klassenvariablen geschieht nur einmal 
        // und wird vom ClassLoader implizit synchronisiert
        static final TransmitterLogger INSTANCE = new TransmitterLogger();
    }

    // Verhindere die Erzeugung des Objektes über andere Methoden
    private TransmitterLogger () {
        try {
            positionReconstructionStrategy= Const.TRANSMITTER_POSITION_RECONSTRUCTION_STRATEGY.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    private PositionReconstructionStrategy positionReconstructionStrategy;
    // Eine nicht synchronisierte Zugriffsmethode auf Klassenebene.
    public static TransmitterLogger getInstance () {
        return InstanceHolder.INSTANCE;
    }

    @Override
    public GHPoint getReconstructedPosition(Visitor visitor, TreeMap<Double, TransmitterStation> stationDistanceMap) {
        return positionReconstructionStrategy.getReconstructedPosition(visitor,stationDistanceMap);
    }

}
