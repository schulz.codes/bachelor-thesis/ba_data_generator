package com.dschulz.datagenerator.Visitor;

import com.dschulz.datagenerator.Const;
import com.graphhopper.util.shapes.GHPoint;
import org.apache.log4j.Logger;
import org.reflections.Reflections;

import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by daniel on 14.07.16.
 */
public class VisitorFactory {
    private static VisitorFactory instance;
    // Verhindere die Erzeugung des Objektes über andere Methoden
    private VisitorFactory () {
        behaviourReflections= new Reflections("com.dschulz.datagenerator.Visitor.BehaviourStrategies");

    }

    public static synchronized VisitorFactory getInstance(){
        if(instance==null){
            instance=new VisitorFactory();
        }
        return instance;
    }

    static final Logger logger = Logger.getLogger("Visitor");
    private List<Visitor> VisitorList = Collections.synchronizedList(new ArrayList<Visitor>());
    private List<Visitor> ActiveVisitorList = Collections.synchronizedList(new ArrayList<Visitor>());
    private AtomicLong idCounter = new AtomicLong(0);
    private AtomicLong activeVisitors = new AtomicLong(Const.MAX_VISITORS_SAME_TIME);
    private Reflections behaviourReflections=null;
    private ScheduledExecutorService scheduledVisitorExecutor= Executors.newScheduledThreadPool(Const.MAX_VISITORS_SAME_TIME + 5);
    private ExecutorService visitorExecutor = Executors.newCachedThreadPool();
    private long lastReleaseTime;
    PriorityBlockingQueue<Long> timeFinished= new PriorityBlockingQueue<>();


    public ScheduledExecutorService getScheduledVisitorExecutor() {
        return scheduledVisitorExecutor;
    }
    public ExecutorService getVisitorExecutor(){return visitorExecutor;}

    public synchronized int getVisitorsCount() {
        return VisitorList.size();
    }
    public synchronized int getActiveVisitorsCount(){
        return ActiveVisitorList.size();
    }

    public Reflections getBehaviourReflections() {
        return behaviourReflections;
    }
    public PriorityBlockingQueue<Long> getTimeFinished() {
        return timeFinished;
    }
    public List<Visitor> getActiveVisitorList(){return new ArrayList<>(ActiveVisitorList);}
    public List<Visitor> getVisitorList(){return new ArrayList<>(VisitorList);}
    public long getLastReleaseTime() {
        return lastReleaseTime;
    }

    /**
     * @param center   centerPoint of Creation Circle
     * @param distance Radius of Creation Circle in KM
     * @param count    how many Visitors should be created?
     * @return
     */
    public synchronized List<Visitor> InitVisitors(GHPoint center, double distance, int count) {
        if (VisitorList.size() < Const.MAX_VISITORS_TOTAL) {
            ArrayList<Visitor> tList = new ArrayList<Visitor>();
            long _count = count;
            if (activeVisitors.get()<= _count) {
                _count = activeVisitors.get();
            }else{
                _count=count;

            }
            if((_count+VisitorList.size())>Const.MAX_VISITORS_TOTAL){
                _count=Const.MAX_VISITORS_TOTAL-VisitorList.size();
            }
            activeVisitors.set(activeVisitors.get()-_count);
            for (int i = 0; i < _count; i++) {
                Visitor tVisitor = new Visitor(idCounter.getAndIncrement(), center, distance);
                VisitorList.add(tVisitor);
                ActiveVisitorList.add(tVisitor);
                tList.add(tVisitor);
                if(Const.BATCH_PROCESSING){
                    visitorExecutor.execute(tVisitor);
                }else{
                    if(!Const.TEST) {
                        ScheduledFuture<?> tFuture = this.getScheduledVisitorExecutor().scheduleAtFixedRate(tVisitor, 0, Const.VISITOR_SLEEPTIME, TimeUnit.MILLISECONDS);
                        tVisitor.setFuture(tFuture);
                    }
                }
            }
            return tList;
        }else{
            return new ArrayList<>();
        }
    }

    public synchronized void ReturnHome(Visitor visitor){
        activeVisitors.getAndIncrement();
        ActiveVisitorList.remove(visitor);
        if(ActiveVisitorList.size()==0 && VisitorList.size()>=Const.MAX_VISITORS_TOTAL){
            logger.info("all Visitors Simulated");
            getScheduledVisitorExecutor().shutdown();
            getVisitorExecutor().shutdown();
        }
    }
}
