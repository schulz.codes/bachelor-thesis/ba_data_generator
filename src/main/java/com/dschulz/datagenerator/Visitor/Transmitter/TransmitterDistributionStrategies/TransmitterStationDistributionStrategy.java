package com.dschulz.datagenerator.Visitor.Transmitter.TransmitterDistributionStrategies;

import com.dschulz.datagenerator.Visitor.Transmitter.TransmitterStation;

import java.util.List;

/**
 * Created by daniel on 15.06.16.
 */
public interface TransmitterStationDistributionStrategy {

    List<TransmitterStation> createStations(double fillFactor);
}
