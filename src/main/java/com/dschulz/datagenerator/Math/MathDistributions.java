package com.dschulz.datagenerator.Math;

import com.graphhopper.util.shapes.GHPoint;

import java.util.Random;

/**
 * Created by daniel on 08.04.16.
 */
public class MathDistributions {

    protected static Random random = new Random();

    public static double RandomInRange(double min, double max) {
        double range = max - min;
        double scaled = random.nextDouble() * range;
        double shifted = scaled + min;
        return shifted; // == (rand.nextDouble() * (max-min)) + min;
    }

    public static double NormalRandom(double mean, double derivation) {
        double rand = (random.nextGaussian() * derivation)+mean;
        return rand;
    }

    public static double EqualRandom(double min, double max) {
        double rand = random.nextDouble() * (max - min) + min;
        return rand;
    }

    public static GHPoint EqualInCircle(GHPoint center, double radius){
        Double xAxis=EqualRandom(-1,1);
        Double yAxis=EqualRandom(-1,1);
        Double angle1=0.0;
        Double angle2=0.0;
        if(xAxis<0){
            angle1=3*(Math.PI/2);//Visitor moves left
            xAxis=xAxis*-1;
        }else{
            angle1=(Math.PI/2);//Visitor moves right
        }
        if(yAxis<0){
            angle2=Math.PI;//Visitor moves down
            yAxis=yAxis*-1;
        }else{
            angle2=0.0;//Visitor moves up
        }
        Double alreadyMoved=radius*xAxis;
        GHPoint xPos=Functions.MoveDistanceInDirection(center,angle1,alreadyMoved);
        //Aus dem Satz des Thales folgt:
        // Konstruiert man ein Dreieck aus den beiden Endpunkten des Durchmessers eines Halbkreises (Thaleskreis) und
        // einem weiteren Punkt dieses Halbkreises, so erhält man immer ein rechtwinkliges Dreieck.
        //
        // Der Höhensatz besagt, dass in einem rechtwinkligen Dreieck das Quadrat über der Höhe flächengleich dem Rechteck aus den Hypotenusenabschnitten ist.
        Double p=radius+alreadyMoved;
        Double q= (2*radius)-p;
        Double h_2=p*q;
        Double h= Math.sqrt(h_2);

        Double endMove= radius*yAxis;
        if(endMove>h){
            return EqualInCircle(center,radius);
        }
        GHPoint finalPos=Functions.MoveDistanceInDirection(xPos,angle2,endMove);
        return finalPos;

    }

}
